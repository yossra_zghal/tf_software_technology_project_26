const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectSchema = new Schema(
  {
    title: { type: String, required: true },
    description: { type: String },
    image: { type: String },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

const Object = mongoose.model("Object", ObjectSchema);
module.exports = Object;
