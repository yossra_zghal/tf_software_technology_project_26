const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoIncrement = require("mongoose-auto-increment");

const CategorySchema = new Schema({
    pictureCategory:{ type: String }   ,
    nameCategory: { type: String, required: true }   
    
  }, {
    _id: false ,
    timestamps: true,
  });
  autoIncrement.initialize(mongoose.connection);
  CategorySchema.plugin(autoIncrement.plugin, "Category");
  var Category = mongoose.model("Category", CategorySchema);
  module.exports = Category;