const { sendNotification } = require("../services/pushNotifications");
const mongoose = require("mongoose");
const emitter = require("../services/eventHandler/eventEmitter");
const { USER_GRADE_UPDATED } = require("../services/eventHandler/eventKeys");

const { Schema } = mongoose;


const UserSchema = Schema({
  avatar: { type: String },
  first_name: { type: String },
  last_name: { type: String },
  email: { type: String },
  bio:{type:String},
  password: { type: String },
  score:{type :Number ,default:0},
  grade:  { type: Number,default:0, ref: 'Grade' },
  bookmarks :   [{ type: mongoose.Schema.ObjectId, ref: "Post" }],
  dateOfBirth: { type: Date },
  telephoneNumber: { type: String },
  resetPasswordToken: { type: String },
  resetPasswordExpires: { type: Date },
  pushToken: { type: String }
});

UserSchema.pre('remove', function(next) {
  // 'this' is the client being removed. Provide callbacks here if you want
  // to be notified of the calls' result.
  // Submission.remove({client_id: this._id}).exec();
  mongoose.models.Post.remove({ author: this._id }).exec()
  next();
});

// UserSchema.pre('save', (next) => { 
//   console.log("PRE SAVE")
//   console.log(this)
//   this.lastGrade = this.grade;
//    next();
//    })
// UserSchema.post('save', () => {
//   if (this.lastGrade !== this.grade) emitter.emit(USER_GRADE_UPDATED, this._id);
// })

UserSchema.methods = {
  sendNotification: message => sendNotification(message, this.pushToken)
};

const User = mongoose.model("User", UserSchema);
module.exports = User;
