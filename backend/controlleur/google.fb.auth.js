import passport from 'passport';
import FacebookStrategy from 'passport-facebook';
import GoogleStrategy from 'passport-google-oauth20';
import User from '../models/user.model';
// Import Facebook and Google OAuth apps configs
import { facebook, google } from '../config/fbGoogleConfig';

// Transform Facebook profile because Facebook and Google profile objects look different
// and we want to transform them into user objects that have the same set of attributes
const transformGoogleProfile = (profile) => ({
  oauth_id: profile.id,
  name: profile.name,
  avatar: profile.picture,
  email:profile.email
});

// Transform Google profile into user object
const transformFacebookProfile = (profile) => ({
  oauth_id: profile.id,
  last_name: profile.last_name,
  first_name: profile.first_name,
  avatar: profile.picture.data.url,
});

// Register Facebook Passport strategy
passport.use(new FacebookStrategy(facebook,
  // Gets called when user authorizes access to their profile
  async (accessToken, refreshToken, profile, done)
 
    // Return done callback and pass transformed user object
    =>  {
      console.log(JSON.stringify(profile))
      console.log(profile._json);  

      done(null, await createOrGetUserFromDatabase(transformFacebookProfile(profile._json)))

  }
));

// Register Google Passport strategy
passport.use(new GoogleStrategy(google,
  
  async (accessToken, refreshToken, profile, done)
    => {
      console.log(profile._json);  
      done(null, await createOrGetUserFromDatabase(transformGoogleProfile(profile._json)))}
    

)
);

const createOrGetUserFromDatabase = async (userProfile) => {
  console.log(userProfile)

  let user = await User.findOne({ email: userProfile.oauth_id }).exec();
  if (!user) {
    user = new User({
      email: userProfile.oauth_id,
      first_name: userProfile.first_name,
      avatar: userProfile.avatar,
      last_name:userProfile.last_name
    });
    console.log(user)
    await user.save();
  }
  return user;
};

// Serialize user into the sessions
passport.serializeUser((user, done) => done(null, user));

// Deserialize user from the sessions
passport.deserializeUser((user, done) => done(null, user));

// Facebook
export const facebookLogin = passport.authenticate('facebook');
export const facebookMiddleware = passport.authenticate('facebook', { failureRedirect: '/auth/facebook' });

// Google
export const googleLogin = passport.authenticate('google', { scope: ['email','profile'] });
export const googleMiddleware = passport.authenticate('google', { failureRedirect: '/auth/google' });

// Callback
export const oauthCallback = async (req, res) => {
  console.log('hello hello ')
  console.log(req.user)

 return  res.redirect('exp://localhost:19000');
};
//://login?user=' + JSON.stringify(req.user)