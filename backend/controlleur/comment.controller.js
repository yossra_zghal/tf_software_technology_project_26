var Post = require("../models/post.model");
import emitter from '../services/eventHandler/eventEmitter'
import { POST_COMMENTED ,POST_LIKED } from '../services/eventHandler/eventKeys';
module.exports = {
  
  like: async (req, res) => {
    console.log(req.params);


 Post.findById(req.params.id).then((Post) => {
      let X = Post.likes_number;
      Post.likes_number = X + 1;
      Post.save();
    }); 

    Post.findByIdAndUpdate(req.params.id)
   .update({ $addToSet: { likes: req.user._id } })
      .exec((err, result) => {
        if (err) {
          return res.status(400).json({
          });
        }
        emitter.emit(POST_LIKED,req.params.id, req.user._id)

        return res.send({
          success: true,
          message: "LIKED  ",
          userId:req.user._id,
          postId:req.params.id
        });
      });
  }
,
  unlike: async (req, res) => {
    console.log(req.params);
    Post.findById(req.params.id).then((Post) => {
      let X = Post.likes_number;
      Post.likes_number = X - 1;
      Post.save();
    }); 

    Post.findByIdAndUpdate(
      req.params.id,
      { $pull: { likes: req.user._id } },
      { new: true }
    ).exec((err, result) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
          // error: errorHandler.getErrorMessage(err) ara saretch iajout:p
          err,
        });
      }

      res.json({
        success: true,
        userId:req.user._id,
        postId:req.params.id
      });
    });
  },
  showAll: async (req, res) => {
    Post.findById(req.params.id)
      .populate("comments.postedBy")
      .then((Post) => {
        var comments = Post.comments;
        Post.save()

          .then(() =>
            res.json({
              success: true,
              message: "List Comments",
              comments: comments,
            })
          )
          .catch((err) => res.status(400).json("Error: " + err));
      });
  },
  comment: async (req, res) => {
   
    Post.findById(req.params.id).then((Post) => {
      let X = Post.comments_number;
      Post.comments_number = X + 1;
      Post.save();
    });
    let comment = req.body.comantaire;
    Post.findByIdAndUpdate(
      req.params.id,
      { $push: { comments: { text: comment, postedBy: req.user._id } } },
      { new: true }
    ).populate("comments.postedBy")
    .select({ "comments": { "$slice": -1 }})
      .exec((err, result) => {

        if (err) {
          console.log(err);
          return res.status(400).json({
            err,
          });
          
        }
        console.log(result)
        emitter.emit(POST_COMMENTED,result._id, result.comments[0]._id)
        res.json({
          success: true,
          comment:result.comments[0],
          userId:req.user._id,
          postId:req.params.id
        });
      });
  },

  uncomment: async (req, res) => {
    Post.findById(req.params.id).then((Post) => {
      let X = Post.comments_number;
      Post.comments_number = X - 1;
      Post.save();
    });
    Post.findByIdAndUpdate(
      req.params.id,
      { $pull: { comments: { _id: req.body.commentId } } },
      { new: true }
    )
      .exec((err, result) => {
        console.log(result)

        if (err) {
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err),
          });
        }
        res.json({
          success: true,
          commentId:req.body.commentId,
          postId:req.params.id

        });
      });
  },
};
