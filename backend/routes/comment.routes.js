const express       = require('express');
const router = express.Router();
const Post          = require('../models/post.model')
const commentController =require('../controller/comment.controller')
const  authenticate = require('../middleware/auth')


router.get('/post/comment/all/:id',  authenticate  , commentController.showAll)

router.post('/post/comment/create/:id',  authenticate  , commentController.comment)

router.post('/post/like/create/:id',  authenticate  , commentController.like)

router.post('/post/unlike/:id',  authenticate  , commentController.unlike)

router.post('/post/comment/delete/:id',  authenticate  , commentController.uncomment)


router.get('/comments/all',authenticate, async (req,res) => {
 
    try {
        const post = await Post.find( {
            $and: [
                  
                   { author: req.user._id },
                   {type:'lost'}
                 
                 ]
          } )
        if(!post){
            return res.status(404).send()
        }
       return res.send({
		success: true,
		message: 'List Post',
		posts:post
	});
    } catch (error) {
		res.status(500).send()
	}
});
module.exports = router