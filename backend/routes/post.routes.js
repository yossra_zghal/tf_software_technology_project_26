const express       = require('express');
const router = express.Router();
const Post          = require('../models/post.model')
const User=require('../models/user.model')
const  authenticate = require('../middleware/auth')
const {ObjectID}    = require('mongodb');

import emitter from '../services/eventHandler/eventEmitter'
import { USER_GRADE_UPDATED } from '../services/eventHandler/eventKeys';

 Post.watch().
    on('change', data => 
    {
        console.log(new Date(), data.operationType) 

        if(data.operationType=='insert'){
    
    User.findById({ _id: data.fullDocument.author })
    .then(user => {
        user.score+=25
        const lastGrade = user.grade;
        let  score = user.score;
        switch (true) {
            case (score < 100):
              user.grade= 0;break;
            case (score >= 100 && score < 500): user.grade= 1;  break;
            case (score >= 500 && score < 1000):user.grade= 2; break;
            case (score >= 1000 && score < 2000):user.grade= 3;break;
            case (score >= 2000 && score < 5000):user.grade= 4;break;
            default:         break;
        } 
       user.save().then(() => {
         if (user.grade !== lastGrade) emitter.emit(USER_GRADE_UPDATED, user._id);
       })

    })
    }
    
   }  );  
    


// -------------- Protected Routes -------------- //
router.post('/post/create',authenticate,  async (req,res) => {

    try {
        var post =  new Post({
	
            ...req.body,
            author: req.user._id
        })
        console.log('before save');
        let savePost = await post.save(); //when fail its goes to catch
        return res.send({
            success: true,
            message: 'Post Aded ',
            post:post
        });
        console.log('after save');
      } catch (err) {
        console.log('err' + err);
        res.status(500).send({
            success: false,
            message: 'adding Post failed please check uncompleted fields'
        });;
      }

	
     
});
router.get("/post/datalist/data",authenticate,  async (req,res) => {
    const posts = await Post.find({})
    var query = JSON.parse(req.query.response);
    
    let page=query.page
    let perPage=query.perPage

     
    let totalPages = Math.ceil(posts.length / perPage)

    if (page !== undefined && perPage !== undefined) {

      let calculatedPage = (page - 1) * perPage
      let calculatedPerPage = page * perPage

      res.status(200).json({
        data: posts.slice(calculatedPage, calculatedPerPage), 
        totalPages
        });
   
    } else {


       return res.status(200).json({
            data: posts.slice(0, 4), 
            totalPages: Math.ceil(posts.length / 4)
            }); 
   
    }
  })

  router.post('/post/create', authenticate, (req, res) => {

    console.log(req.body)
    var post = new Post({

        ...req.body,
        author: req.user._id
    })

    post.save()



    return res.send({
        success: true,
        message: 'Post Aded ',
        post: post
    });

});



router.get('/posts/all',authenticate, async (req,res) => {
 
           await Post.find({publish_status:true})
        .populate('author')
        .populate('comments.postedBy')
        .exec((err, result) => {
            if (err) {
              console.log(err)
              return res.status(400).json({
              err
              })
            }
            res.json({
                success: true,
                message: 'List Post',
                length:result.length,
                posts:result
            })
          }) 

});





router.get('/posts/lost/all',authenticate, async (req,res) => {
 
    try {
        const post = await Post.find( {
            $and: [
                  
                   { author: req.user._id },
                   {type:'lost'}
                 
                 ]
          } )
        if(!post){
            return res.status(404).send()
        }
       return res.send({
		success: true,
		message: 'List Post',
		posts:post
	});
    } catch (error) {
		res.status(500).send()
	}
});


router.get('/posts/found/all',authenticate, async (req,res) => {
 
    try {
        const post = await Post.find( {
            $and: [
                  
                   { author: req.user._id },
                   {type:'found'}
                 
                 ]
          } )
        if(!post){
            return res.status(404).send()
        }
       return res.send({
		success: true,
		message: 'List Post',
		posts:post
	});
    } catch (error) {
		res.status(500).send()
	}
});


router.delete('/post/delete/:id', authenticate,async (req,res) => {
    const _id = req.params.id
    if (!ObjectID.isValid(_id)) {
        return res.status(404).send();
    }
    try {
        const deletepost = await Post.findOneAndDelete({_id:_id})
        if (!deletepost) {
            return res.status(404).send();
        }

        res.json({
            success: true,
            deletePost :_id
        })
    } catch (error) {
        res.status(500).send()
    }
})

router.patch('/posts/update/:id',authenticate, async (req, res) => {
    console.log("body "+    JSON.stringify(req.body))

        Post.findByIdAndUpdate(req.params.id, {$set: {...req.body}})
            .then(() => res.json('Post updated!'))
            .catch(err => res.status(400).json('Error: ' + err));


    
})


router.get('/countPets',/*authenticate,*/ async (req, res) => {
    try {
        const x = Post.find({ category: '2' }).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countPets: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
});

router.get('/countHuman',/*authenticate,*/ async (req, res) => {
    try {
        const x = Post.find({ category: '1' }).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countHuman: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
});

router.get('/countItem',/*authenticate,*/ async (req, res) => {
    try {
        const x = Post.find({ category: '3' }).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countItem: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
});
router.get('/countPost',/*authenticate,*/ async (req, res) => {
    try {
        const x = Post.find({}).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countpost: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
}

);

router.get('/countPostL',/*authenticate,*/ async (req, res) => {
    try {
        const x = Post.find({ type: 'lost' }).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countpostL: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
});
router.get('/countPost',/*authenticate,*/ async (req, res) => {
    try {
        const x = Post.find({}).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countpost: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
}

);

router.get('/countPostPublished'/*,authenticate*/, async (req, res) => {
    try {
        const x = Post.find({ publish_status: true }).count(function (err, count) {

            if (err) throw err;

            return res.send({
                success: true,
                countpostpublished: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
});

router.get('/countPostNotPublished'/*,authenticate*/,async (req, res) => {
    try {
        const x = Post.find({ publish_status: false }).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                countpostnotPublished: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
});


router.get('/countUser',/*authenticate,*/ async (req, res) => {
    try {
        const x = User.find({}).count(function (err, count) {
            if (err) throw err;

            return res.send({
                success: true,
                count: count
            });
        });

    }
    catch (error) {
        res.status(400).send(error)
    }
}
);





router.post('/saveBookmarks/:id'  , authenticate,async (req, res) => {
  
  
    const favoritePost = req.params.id
    User.findByIdAndUpdate(
        req.user._id,
      { $push: { bookmarks: favoritePost } },
      { new: true }
    ).populate("bookmarks")
    .select({ "bookmarks": { "$slice": -1 }})
      .exec((err, result) => {
          console.log(result.bookmarks[0]._id)
        if (err) {
          console.log(err);
          return res.status(400).json({
            err,
          });
        }
        res.json({
            success: true,
            favoritePost :result.bookmarks[0]._id
        })
      });
  });

  router.post('/unsaveBookmarks/:id'  , authenticate,async (req, res) => {
  
  
    const favoritePost = req.params.id

    User.findByIdAndUpdate(
        req.user._id,
        { $pull: { bookmarks: favoritePost } },
        { new: true }
      )
      .exec((err, result) => {
        if (err) {
          console.log(err);
          return res.status(400).json({
            err,
          });
        }
        res.json({
            success: true,
            favoritePostDeleted :req.params.id
        })
      });
  });
  router.get('/objectPerWeek', async (req, res) => {
    console.log("dsfsdf")

  })
  router.get('/ParMonth', async (req, res) => {
    var i;
    var PeriodOfTime = [];
    var ResultTab = [];
    var arrayStatestique = [];
    var PeriodOfTimeMonth = [];
    var monthName = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    var d = new Date();
    var oneYearFromNow = new Date();
    var Aujourdhui = new Date();
    oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() - 1);
    d.setDate(1);
    for (i = 0; i <= 11; i++) {
        PeriodOfTime[i] = d.getMonth() + 1 + ' ' + d.getFullYear();
        PeriodOfTimeMonth[i] = monthName[d.getMonth()] + ' ' + d.getFullYear();
        d.setMonth(d.getMonth() - 1);
    }
    Post.aggregate(
        [{
            $match: {
                'createdAt': { $gte: oneYearFromNow, $lt: Aujourdhui }
            },
        },
        {
            $project: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" },
                day: { $dayOfMonth: "$createdAt" },
            },
        },

        {
            $group: {
                _id: {
                    year: "$year",
                    month: "$month",
                    // day: "$day",
                },
                count: { $sum: 1 },
            }
        },
        { $sort: { _id: 1 } }
        ],
        function (err, result) {
            result.map(r => {
                ResultTab.push(r);
            })
            for (i = 0; i < PeriodOfTime.length; i++) {
                if (ResultTab.find(item => item._id.month + ' ' + item._id.year === PeriodOfTime[i])) {
                    arrayStatestique.push(ResultTab.find(item => item._id.month + ' ' + item._id.year === PeriodOfTime[i]).count)
                }
                else arrayStatestique.push(0)
            }

            if (err) {
                res.send(err);
            } else {
                return res.send({
                    success: true,
                    arrayStat: arrayStatestique,
                    Month: PeriodOfTimeMonth,
                });
            }
        }
    );





});
  
module.exports = router