const express       = require('express');
const router = express.Router();
const Item          = require('../models/object.model')
const User          = require('../models/user.model')
const  authenticate = require('../middleware/auth')
const {ObjectID}    = require('mongodb')
const { sendNotification } = require("../services/pushNotifications");
var moment = require('moment'); // require


// -------------- Protected Routes -------------- //
router.post('/object/create',authenticate,  (req,res) => {
console.log(req.body)
    var object=  new Item({
        ...req.body,
        author: req.user._id
    })
    
	object.save()
	return res.send({
		success: true,
		message: 'Object Added ',
		object:object
	});

});



router.get('/objects/all',authenticate, async (req,res) => {
 
    try {
        const object = await Item.find({ author: req.user._id })
        if(!object){
            return res.status(404).send()
        }
       return res.send({
		success: true,
		message: 'Ojects List',
		objects:object
	});
    } catch (error) {
		res.status(500).send()
	}
});
router.delete('/object/delete/:id', authenticate,async (req,res) => {
    const _id = req.params.id
    if (!ObjectID.isValid(_id)) {
        return res.status(404).send();
    }
    try {
        const deleteobject = await Item.findOneAndDelete({_id:_id})
        if (!deleteobject) {
            return res.status(404).send();
        }
        res.send(deleteobject)
    } catch (error) {
        res.status(500).send()
    }
})


router.get('/object/scan/:id', authenticate,async (req,res) => {
    var object = await Item.findById({ _id: req.params.id }).populate('author');
     var objectAuthor = await User.findById(object.author);
     var message = {
        sound: 'default',
        body: `${[req.user.first_name, req.user.last_name].join(' ')}has found your object ${object.title}`,
        data: {
             message: `${[req.user.first_name, req.user.last_name].join(' ')} has found your object ${object.title}`,
             sender: req.user._id,    
             },
      };
      sendNotification(message, objectAuthor.pushToken)
    /*  objectAuthor.sendNotification({
        sound: 'default',
        body: `${req.user.username} has found your object ${object.Title}`,
      }); */
      console.log(object)
      return res.send({
		success: true,
		message: 'Object scanned ',
		object:object
	});
})

function formatDate(date){

  var dd = date.getDate();
  var mm = date.getMonth()+1;
  var yyyy = date.getFullYear();
  // if(dd<10) {dd='0'+dd}
  // if(mm<10) {mm='0'+mm}
  date = mm+' '+dd+' '+yyyy;
  return date
}
router.route("/staticObject").get(function (req, res) {
  var ResultTab = [];
  var arrayStatestique = [];

 
  var last7Day = [];
    for (var i=0; i<7; i++) {
        var d = new Date();
        d.setDate(d.getDate() - i);
        last7Day.push( formatDate(d) )
    }
 
    console.log(last7Day)
  
  
  Item.aggregate(
      [{
        $match: {
            'createdAt': {$gte: new Date((new Date().getTime() - (7 * 24 * 60 * 60 * 1000)))}
        },
    },
        {
          $project: {
            year: { $year: "$createdAt" },
            month: { $month: "$createdAt" },
            day: { $dayOfMonth: "$createdAt" },
          },
        },
  
        {
          $group: {
            _id: {
              year: "$year",
              month: "$month",
              day: "$day",
            },
            count: { $sum: 1 },
          }
        },
          { $sort: { _id: 1 } }
      ],
      function (err, result) {
        if (err) {
          res.send(err);
        } else {

          result.map(r => {
            ResultTab.push(r);
        })

        for (i = 0; i < last7Day.length; i++) {
          console.log(last7Day[i])
          if (ResultTab.find(item => item._id.month + ' '+item._id.day + ' ' + item._id.year === last7Day[i])) {
              arrayStatestique.push(ResultTab.find(item => item._id.month + ' '+item._id.day + ' ' + item._id.year === last7Day[i]).count)
          }
          else arrayStatestique.push(0)
      }
console.log(arrayStatestique)



        return res.send({
          success: true,
          arrayStat: arrayStatestique,
          Week: last7Day,
        });





        }
      }
    );
    })

module.exports = router