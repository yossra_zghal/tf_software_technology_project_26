const auth = require("../controller/auth.controllers");
const errors = require("../controller/error");
const passport = require("passport");
const authenticate = require("../middleware/auth");
import {
  facebookLogin,
  facebookMiddleware,
  googleLogin,
  googleMiddleware,
  oauthCallback,
} from "../controller/google.fb.auth";
const User = require('../models/user.model');

module.exports = (app) => {
  /**
 * @swagger
 * /signup:
 *   post:
 *     tags:
 *       - Users
 *     name: Register
 *     summary: Register a new user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *           type: object
 *           properties:
 *             first_name:
 *               type: string
 *             last_name:
 *               type: string
 *             email:
 *               type: string
 *             password:
 *               type: string
 *               format: password
 *         required:
 *           - email
 *           - password
 *     responses:
 *       '200':
 *         description: User created
 *       '403':
 *         description: Username or email already taken
 */

	app.post("/signup", (req, res) => {
    console.log(req.body);
    auth
      .registerUser(
        req.body.first_name,
        req.body.last_name,
        req.body.email,
        req.body.password,
        req.body.avatar
      )
      .then((user) => {
        res.send({
          user,
          success: true,
        });
      })
      .catch((err) => {
        return errors.errorHandler(res, err);
      });
  });


  app.post("/signupFacebook",  async (req, res) => {

    await User.find({ email: req.body.email    })
    .exec((err, result) => { 
console.log("resultat de fin par email ",result.length)
      if (result.length === 0) { 
          
          auth
            .registerUser(
             req.body.first_name,
              req.body.last_name,
              req.body.email,
              req.body.password,
              req.body.avatar
      
            ).then((user) => { 
              auth
            .loginUser(req.body.email, req.body.password)
            .then((user) => {
              let token = auth.createToken(user);
              return Promise.all([
                token
              ]).then((tokens) => {
                return {
                  user: user,
                  token: tokens[0]
                };
              });
            })
            .then((success) => {
              res.send({
                success: true,
                token: success.token,
                user: success.user,
                message: "user found & logged in",
              });
            })
            .catch((err) => {
              return console.log(err)
            });
            
        });
          
       }else {
          auth
          .loginUser(req.body.email, req.body.password)
          .then((user) => {
            let token = auth.createToken(user);
            return Promise.all([
              token
            ]).then((tokens) => {
              return {
                user: user,
                token: tokens[0]
              };
            });
          })
          .then((success) => {
            res.send({
              success: true,
              token: success.token,
              user: success.user,
              message: "user found & logged in",
            });
          })
          .catch((err) => {
            return console.log(err)
    
          });
        } 
    })
    
});

app.post("/signupGoogle",  async (req, res) => {

  await User.find({ email: req.body.email    })
  .exec((err, result) => { 
console.log("resultat de fin par email ",result.length)
    if (result.length === 0) { 
        
        auth
          .registerUser(
           req.body.first_name,
            req.body.last_name,
            req.body.email,
            req.body.password,
            req.body.avatar
    
          ).then((user) => { 
            auth
          .loginUser(req.body.email, req.body.password)
          .then((user) => {
            let token = auth.createToken(user);
            return Promise.all([
              token
            ]).then((tokens) => {
              return {
                user: user,
                token: tokens[0]
              };
            });
          })
          .then((success) => {
            res.send({
              success: true,
              token: success.token,
              user: success.user,
              message: "user found & logged in",
            });
          })
          .catch((err) => {
            return console.log(err)
          });
          
      });
        
     }else {
        auth
        .loginUser(req.body.email, req.body.password)
        .then((user) => {
          let token = auth.createToken(user);
          return Promise.all([
            token
          ]).then((tokens) => {
            return {
              user: user,
              token: tokens[0]
            };
          });
        })
        .then((success) => {
          res.send({
            success: true,
            token: success.token,
            user: success.user,
            message: "user found & logged in",
          });
        })
        .catch((err) => {
          return console.log(err)
  
        });
      } 
  })
  
});



  /**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *       - Users
 *     name: Login
 *     summary: Logs in a user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *               format: password
 *         required:
 *           - email
 *           - password
 *     responses:
 *       '200':
 *         description: User found and logged in successfully
 *       '401':
 *         description: Bad username, not found in db
 *       '403':
 *         description: Username and password don't match
 */

  app.post("/login", (req, res) => {
    auth
      .loginUser(req.body.email, req.body.password)
      .then((user) => {
        let token = auth.createToken(user);
        return Promise.all([
          token
        ]).then((tokens) => {
          return {
            user: user,
            token: tokens[0]
          };
        });
      })
      .then((success) => {
        res.send({
          success: true,
          token: success.token,
          user: success.user,
          message: "user found & logged in",
        });
      })
      .catch((err) => {
        return errors.errorHandler(res, err);
      });
  });

  // Set up facebook and google auth routes
  app.get("/auth/facebook", facebookLogin);
  app.get("/auth/facebook/callback", facebookMiddleware, oauthCallback);
  
  app.get("/auth/google", googleLogin);
  app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/auth/google' }),
  (req, res) => res.redirect('exp://192.168.1.10:19000://login?user=' + JSON.stringify(req.user)));

  //app.get("/auth/google/callback", googleMiddleware, oauthCallback);

  app.put("/pushToken", authenticate, ({ user, body: { pushToken } }) => {
    user.pushToken = pushToken;
    return user.save();
  });

  app.post("/refreshToken", (req, res) => {
    auth
      .validateRefreshToken(req.body.refreshToken)
      .then((tokenResponse) => {
        return auth.createToken(tokenResponse);
      })
      .then((authToken) => {
        res.status(200).send({
          success: true,
          authToken: authToken,
        });
      })
      .catch((err) => {
        if (err.code) {
          return errors.errorHandler(res, err.message, err.code);
        } else {
          return errors.errorHandler(res, err.message);
        }
      });
  });
  // Customize auth message Protect the  routes

  /*  app.all('*', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user) => {
		console.log(user)
        if (err || !user) {
            const error = new Error('You are not authorized to access this area');
            error.status = 401;
            throw error;
        }

        //
        req.user = user;
        return next();
    })(req, res, next);
}); 
 */
};
