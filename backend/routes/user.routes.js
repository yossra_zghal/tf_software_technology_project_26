const passport = require("passport");
const User = require("../models/user.model");
const errors = require("../controller/error");
const authenticate = require("../middleware/auth");
const { ObjectID } = require("mongodb");
import emitter from '../services/eventHandler/eventEmitter'
import { OFFLINE_NOTIFY } from '../services/eventHandler/eventKeys';

module.exports = (app) => {
  /**
   * @swagger
   * definitions:
   *   User:
   *     type: object
   *     properties:
   *       avatar:
   *         type: string
   *       first_name:
   *         type: string
   *       last_name:
   *         type: string
   *       dateOfBirth:
   *         type: string
   *         format: date-time
   *       email:
   *         type: string
   *       score:
   *         type: integer
   *       grade:
   *         type: integer
   *       password:
   *         type: string
   *         format: password
   *       resetPasswordToken:
   *         type: string
   *       resetPasswordExpires:
   *         type: string
   *         format: date-time
   *       required:
   *         - email
   *         - first_name
   *         - password
   *         - last_name
   *
   */

  /**
   * @swagger
   * /user/profile:
   *   get:
   *     tags:
   *       - Users
   *     name: Find user
   *     summary: Finds a user
   *     security:
   *       - bearerAuth: []
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     responses:
   *       '200':
   *         description: A single user object
   *         schema:
   *           $ref: '#/definitions/User'
   *       '401':
   *         description: No auth token / no user found in db with that name
   *       '403':
   *         description: JWT token and username from client don't match
   */

  app.get("/user/profile", authenticate, async (req, res) => {
    console.log("fetch l user");

    try {
      const user = await User.findById({ _id: req.user._id }).populate("grade");
      if (!user) {
        return res.status(404).send();
      }
      return res.send({
        success: true,
        message: "User Profile ",
        user: user,
      });
    } catch (error) {
      res.status(500).send();
    }
  });

  app.get("/user/find/:id", authenticate, async (req, res) => {
    try {
      const user = await User.findById({ _id: req.params.id });

      res.send({
        success: true,
        message: "get user",
        user: user,
      });
    } catch (error) {
      res.status(500).send();
    }
  });

  /**
   * @swagger
   * /updateUser:
   *   put:
   *     tags:
   *       - Users
   *     name: Update User
   *     summary: Update user info
   *     security:
   *       - bearerAuth: []
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: body
   *         in: body
   *         schema:
   *           $ref: '#/definitions/User'
   *           type: object
   *           properties:
   *             first_name:
   *               type: string
   *             last_name:
   *               type: string
   *             email:
   *               type: string
   *         required:
   *           - username
   *     responses:
   *       '200':
   *         description: User info updated
   *       '403':
   *         description: No authorization / user not found
   */

  app.patch("/user/update", authenticate, async (req, res) => {

    try {
      const updatedUser = await User.findByIdAndUpdate(req.user._id, {
        $set: { ...req.body },
      });
      res.send({
        success: true,
        user: updatedUser,
      });

    } catch (error) {
      res.status(500).send();
    }
   
 
  });

  /**
   * @swagger
   * /deleteUser:
   *   delete:
   *     tags:
   *       - Users
   *     name: Delete User
   *     summary: Delete user
   *     security:
   *       - bearerAuth: []
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     responses:
   *       '200':
   *         description: User deleted from db
   *       '403':
   *         description: Authentication error
   *       '404':
   *         description: No user in db with that name
   *       '500':
   *         description: Problem communicating with db
   */

  app.delete("/deleteUser", authenticate, async (req, res) => {
    if (!ObjectID.isValid(req.user._id)) {
      return res.status(404).send();
    }

    try {
      await req.user.remove();
      res.send(req.user);
    } catch (error) {
      res.status(500).send();
    }
  });


  app.get('/user/all',/*authenticate,*/ async (req,res) => {
   await User.find({})
    .exec((err, result) => {
     if (err) {
        console.log("lerrur"+err)
       console.log(err)
       return res.status(400).json({
       err
       })
     }
     res.json({
         success: true,
         message: 'All Users',
         length:result.length,
         users:result
     })
   }) 
});


app.post('/requestJoin/:id',authenticate,async (req,res) => {

  emitter.emit(OFFLINE_NOTIFY,req.params.id, req.user._id)

 
  res.status(200).json({
    success:true
    });
});

  
};
