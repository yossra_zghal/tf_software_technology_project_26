const express = require("express");
const router = express.Router();
const Grade = require("../models/grade.model");
const authenticate = require("../middleware/auth");
const { ObjectID } = require("mongodb");

router.post("/grade/create", (req, res) => {
  console.log(req.body);
  var grade = new Grade({
    ...req.body,
  });

  grade.save();
  return res.send({
    success: true,
    message: "Grade Aded ",
    grade: grade,
  });
});

router.get("/grade/list", authenticate, async (req, res) => {
  await Grade.find({})
    .populate("benefices")
    .exec((err, result) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
          err,
        });
      }
      res.json({
        success: true,
        message: "List Post",
        grades: result,
        length: result.length,
      });
    });
});

router.get("/grade/datalist/data", authenticate, async (req, res) => {
  const grades = await Grade.find({}).populate("benefices");

  var query = JSON.parse(req.query.response);

  let page = query.page;
  let perPage = query.perPage;

  let totalPages = Math.ceil(grades.length / perPage);

  if (page !== undefined && perPage !== undefined) {
    let calculatedPage = (page - 1) * perPage;
    let calculatedPerPage = page * perPage;

    res.status(200).json({
      data: grades.slice(calculatedPage, calculatedPerPage),
      totalPages,
    });
  } else {
    res.status(200).json({
      data: grades.slice(0, 4),
      totalPages: Math.ceil(grades.length / 4),
    });
  }
});
router.patch("/grade/update/:id", authenticate, async (req, res) => {
  delete req.body._id;
  // delete (req.body.benefices)

  console.log(req.body);
  const updates = Object.keys(req.body);
  const allowedUpdates = ["gradeName", "lowPoint", "highPoint", "benefices"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );
  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates" });
  }
  try {
    const post = await Grade.findOne({ _id: req.params.id }).populate(
      "benefices"
    );

    console.log(post);

    if (!post) return res.status(404).send();

    updates.forEach((update) => (post[update] = req.body[update]));
    await post.save();
    res.send(post);
  } catch (error) {
    res.status(400).send(error);
  }
});
router.delete("/grade/delete/:id", authenticate, async (req, res) => {
  const _id = req.params.id;

  try {
    const deletepost = await Grade.findOneAndDelete({ _id: _id });
    if (!deletepost) {
      return res.status(404).send();
    }
    res.send(deletepost);
  } catch (error) {
    res.status(500).send();
  }
});
module.exports = router;
