const express       = require('express');
const router = express.Router();
const  authenticate = require('../middleware/auth')
const Notification          = require('../models/notification.models')

const JSON = require('circular-json');

// -------------- Protected Routes -------------- //
router.post('/save',authenticate,  (req,res) => {
    console.log(req.body)

     var notification=  new Notification({
            ...req.body,
            receiver: req.user._id
        }) 
        notification.save() 
 
        
//    const notifications =  Notification.find({ receiver: req.user._id }) 

        return res.send({
            success: true,
            message: 'notification  Added ',
            notification: notification
        }); 
     
    });

router.get('/all',authenticate, async (req,res) => {
 //{receiver:req.user._id}
     await Notification.find({receiver:req.user._id})
     .populate('sender')
     .exec((err, result) => {
         if (err) {
           console.log(err)
           return res.status(400).json({
           err
           })
         }
         res.json({
             success: true,
             message: 'List Notification',
             notifications:result
         })
       }) 

});



    module.exports = router