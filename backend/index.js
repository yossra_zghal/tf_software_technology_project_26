var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var swaggerUi = require("swagger-ui-express");
var swaggerJSDoc = require("swagger-jsdoc");
var express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");
const Cors = require("cors");
const helmet = require("helmet");
const socketIO = require("socket.io");
const http = require("http");
const app = express();
const postRoute = require("./routes/post.routes");
const objectRoute = require("./routes/object.routes");
const gradeRoute = require("./routes/grade.routes");
const commentRoute = require("./routes/comment.routes");
const benifiteRoute = require("./routes/benefice.routes");
const reclamationRoute = require("./routes/reclamation.routes");
const notificationRoute = require("./routes/notifcation.routes");
const partnerRoute = require("./routes/partner.routes");
const categoryrRoute = require("./routes/category.routes");
const giftRoute = require("./routes/gift.routes");
const User=require('./models/user.model');
require('./models/post.model'); 
require('./models/grade.model');
require('./services/eventHandler');
var socketioJwt = require("socketio-jwt");
require("dotenv").config();

const swaggerDefinition = {
  info: {
    title: "FindIt Swagger API",
    version: "1.0.0",
    description: "Endpoints to test the user registration routes",
  },
  host: "localhost:5000",
  basePath: "/",
  securityDefinitions: {
    bearerAuth: {
      type: "apiKey",
      name: "Authorization",
      scheme: "bearer",
      in: "header",
    },
  },
};
// Connexion Base de donnée

mongoose.connect(process.env.MONGO_URI || "mongodb://localhost:27017/testapi", {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

const connection = mongoose.connection;
connection.once("open", () => {
  /*   var user = new User({ _id: '8ihK3dshj5WWJSDMiE85UYa8K8r2',first_name :'admin' ,last_name :'admin' ,email :'admin' ,password:'admin'});
  user.save(function (err, fluffy) {
    if (err) return console.error(err);

  }); */

  console.log("Mongo connextion establich succesfully");
});

const options = {
  swaggerDefinition,
  apis: [__dirname + "/routes/*.js"],
};

const swaggerSpec = swaggerJSDoc(options);

app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});

const whitelist = ["http://localhost:5000", "http://localhost:4000"];
const corsOptions = {
  origin: (origin, callback) => {
    console.log(whitelist.indexOf(origin));
    if (whitelist.indexOf(origin) == -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  optionsSuccessStatus: 200,
};

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

require("./config/passport");
app.use(cookieParser());
app.use(Cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));
app.use(helmet());
app.use(express.json()); // Make sure it comes back as json
app.use(passport.initialize());
app.use(passport.session());


require("./routes/auth.routes")(app);
require("./routes/user.routes")(app);
app.use(postRoute);
app.use(objectRoute);
app.use(gradeRoute);
app.use(commentRoute);
app.use(benifiteRoute);
app.use(reclamationRoute);
app.use(categoryrRoute);
app.use(partnerRoute);
app.use(giftRoute);
app.use("/notifcation", notificationRoute);

const server = http.createServer(app);
const io = socketIO.listen(server);

//let currentUserId = 2;
import { v1 as uuidv1 } from "uuid";
import { AsyncLocalStorage } from 'async_hooks';
const users = {};

 async function  createUserAvatar(id) {
  const user = await User.findById({ _id: id })

  if (user && user.avatar) {
    return user.avatar;
  } 


 
}
function createUsersOnline() {

  const values = Object.values(users);
  const onlyWithUsernames = values.filter((u) => u.username != undefined);
  return onlyWithUsernames;
}

io.sockets
  .on(
    "connection",
    socketioJwt.authorize({
      secret: "kQ6uM7G5RoPGKQG8rUy0EBVInvinVGjml3lHuPvaRxKNZyRtNk2sUHea7fG36Bj",
      timeout: 15000, // 15 seconds to send the authentication message
    })
  )
  .on("authenticated", (socket) => {
    //this socket is authenticated, we are good to handle more events from it.
    console.log("New client connected", socket.id);
    users[socket.id] = { userId: socket.decoded_token._id };
    socket.on("disconnect", () => {
      delete users[socket.id];
      io.emit("action", { type: "users_onlines", data: createUsersOnline() });
    });
    socket.on("action", async (action) => {
      switch (action.type) {
        case "server/join":
          console.log("got joins event ",socket.decoded_token.first_name + " "+ socket.decoded_token.last_name);
          var username =socket.decoded_token.first_name + " "+ socket.decoded_token.last_name
          users[socket.id].username = username;
          users[socket.id].avatar = await createUserAvatar(socket.decoded_token._id); 
          io.emit("action", {
            type: "users_onlines",
            data: createUsersOnline(),
          }); //return back to all  socket or socket emit only to the connected socket
          socket.emit("action", { type: "selfuser", data: users[socket.id] });
          break;
        case "server/private_message":
          const conversationId = action.data.conversationId;
          const from = users[socket.id].userId;
          const userValues = Object.values(users);
          const socketIds = Object.keys(users);
          for (let i = 0; i < userValues.length; i++) {
            if (userValues[i].userId === conversationId) {
              const socketId = socketIds[i];
              io.sockets.sockets[socketId].emit("action", {
                type: "private_message",
                data: {
                  ...action.data,
                  conversationId: from,
                },
              });
              break;
            }
          }
          break;
      }
    });
  });
// eslint-disable-next-line no-console
const PORT = process.env.PORT || 5000;
server.listen(PORT, () => console.log(`Listening on port ${PORT}`));

module.exports = server;
//    "node ./node_modules/babel-cli/bin/babel-node  index.js"
//      "nodemon index.js --exec babel-node
