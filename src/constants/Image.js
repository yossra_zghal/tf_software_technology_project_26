const IMAGE = {
    ICON_PROFILE: require('../images/profile.png'),
    ICON_MENU: require('../images/menu.png'),
    ICON_BACK: require('../images/back.png'),
    ICON_QR_CODE :require('../images/qrcode-512.png'),
    ICON_QR_CODE_BLACK :require('../images/icons8-qr-code-26.png'),

    ICON_HOME: require('../images/home.png'),
    ICON_HOME_BLACK: require('../images/home-black.png'),

    ICON_SETTINGS: require('../images/settings.png'),
    ICON_SETTINGS_BLACK: require('../images/settings-black.png'),

    ICON_ADD: require('../images/add.png'),
    ICON_ADD_BLACK: require('../images/add-black.png'),
 
    ICON_BELL: require('../images/bell.png'),
    ICON_BELL_BLACK: require('../images/bell_back.png'),

    ICON_SEARCH: require('../images/search.png'),
    ICON_SEARCH_BLACK: require('../images/search-black.png'),

    
    ICON_CHAT_BLACK: require('../images/bleuberry-messenger.png'),
    ICON_CHAT: require('../images/blackberry-messenger.png'),

    ICON_USER: require('../images/user.png'),
    ICON_USER_BLACK: require('../images/user-black.png'),
}

export {IMAGE}