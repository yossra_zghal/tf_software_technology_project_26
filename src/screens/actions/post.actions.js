import {
  ADDING_FAILED, ADDING_SUCCESS, ADDING_POST,LOADING_FAILED, LOADING_SUCCESS, LOADING_POSTS
,ADDING_RECLAMATION,ADDING_RECLAMATION_FAILED,ADDING_RECLAMATION_SUCCESS} from './types';
import { AsyncStorage } from 'react-native';
export const addPost = ({ type, title, category, image, description, location,reward, city }) => {

  return (dispatch) => {

    dispatch({ type: ADDING_POST });

    let postData = JSON.stringify({
      type ,
      title ,
      category,
      description,
      image,
      location,
      reward,
      city

    })
    AsyncStorage.getItem('app_token').then(token => {

      fetch('http://192.168.1.10:5000/post/create', {
        method: 'POST',
        mode: 'cors',
        body: postData,
        headers: new Headers({
          "Authorization": 'Bearer ' + token,
          'Content-Type': 'application/json',
          //'Accept': 'application/json'

        })

      })
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) { dispatch({ type: ADDING_SUCCESS }) }
          else

            dispatch({ type: ADDING_FAILED, error: responseData.message })
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    }
    )
  }
}

export function editPost( title, description, reward,post ) {
  return dispatch => {
    let postData = JSON.stringify({
      title: title,
      description: description,
      reward:reward
    });
    AsyncStorage.getItem("app_token").then(token => {
      fetch("http://192.168.1.10:5000/posts/update/" + post._id, {
        method: "PATCH",
        mode: "cors",
        headers: new Headers({
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
          //'Accept': 'application/json'
        }),
        body: postData
      })
        .then(response => {
          return response.json();
        })
        //  .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) {
            dispatch({ type: LOADING_SUCCESS, posts: responseData.posts });
          } else
            dispatch({ type: LOADING_FAILED, error: responseData.message });
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    });
  };
}

export function deletePost(key) {
  return dispatch => {
    AsyncStorage.getItem("app_token").then(token => {
      fetch("http://192.168.1.10:5000/post/delete/" + key, {
        method: "DELETE",
        mode: "cors",
        headers: new Headers({
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
          //'Accept': 'application/json'
        })
      })
        .then(response => {
          return response.json();
        })
        //  .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) {
            dispatch({ type: LOADING_SUCCESS, posts: responseData.posts });
          } else
            dispatch({ type: LOADING_FAILED, error: responseData.message });
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    });
  };
}

export const fetchPosts = () => {
  return dispatch => {
    dispatch({ type: LOADING_POSTS });

    AsyncStorage.getItem("app_token").then(token => {
      
      fetch("http://192.168.1.10:5000/posts/all", {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
          //'Accept': 'application/json'
        })
      })
        .then(response => {
          return response.json();
        })
        //  .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) {
            dispatch({ type: LOADING_SUCCESS, posts: responseData.posts });
          } else
            dispatch({ type: LOADING_FAILED, error: responseData.message });
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    });
  };
};

export const addReclamation = ({ idpost, mess, value }) => {
  console.log("Ena fel action mta3 post ")
  console.log(value)
  return (dispatch) => {

    dispatch({ type: ADDING_RECLAMATION});

    let postData = JSON.stringify({
     
      message: mess,
      keyword: value
    })
    AsyncStorage.getItem('app_token').then(token => {

      fetch('http://192.168.1.10:5000/reclamation/create/'+idpost, {
        method: 'POST',
        mode: 'cors',
        body: postData,
        headers: new Headers({
          "Authorization": 'Bearer ' + token,
          'Content-Type': 'application/json',
          //'Accept': 'application/json'

        })

      })
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) { dispatch({ type: ADDING_RECLAMATION_SUCCESS }) }
          else

            dispatch({ type: ADDING_RECLAMATION_FAILED, error: responseData.message })
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    }
    )
  }
}

