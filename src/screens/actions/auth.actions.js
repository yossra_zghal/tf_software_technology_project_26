import { AsyncStorage } from 'react-native';
import { generalError } from './error.actions';
import {SET_AUTH_PENDING,SET_LOGIN_SUCCESS,SET_LOGIN_ERROR,SET_LOGOUT,SET_REGISTER_SUCCESS,SET_REGISTER_ERROR,SAVE_APP_TOKEN} from './types'

const _saveItem = async (item, selectedValue) => {
	try {
		await AsyncStorage.setItem(item, selectedValue);
		console.log('saved')
	} catch (error) {
		throw error;
	}
};
 export const logout = () => async dispatch => {	
	 console.log('loged')
	dispatch({ type: SET_LOGOUT });
	try {
		await     AsyncStorage.removeItem("app_token");

	} catch (error) {
		//dispatch(asyncError(error));
	}
};   

export const loginWithFacebook =(profile)=> {

	return (dispatch) => {
 console.log(profile)
	dispatch({ type: SET_AUTH_PENDING });
	ArrayofFirstLastName =[]
	var FullName ="";
	FullName=profile.name;
	ArrayofFirstLastName=	FullName.split(" ");
	let userData = JSON.stringify({
		first_name: ArrayofFirstLastName[0],
		last_name: ArrayofFirstLastName[1],
		email: profile.email,
		avatar: profile.picture.data.url,
		password:profile.id
		
	  })
	return fetch('http://192.168.1.10:5000/signupFacebook', {

			method: 'POST',
			body: userData,
			headers: new Headers({
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			  })	
	})
	.then(response => { return response.json(); })
	.then(responseData => { return responseData; })
	.then(responseData => {
		if (responseData.success) { 
			dispatch({ type: SET_LOGIN_SUCCESS ,authToken:responseData.token});
			_saveItem('app_token', responseData.token)

		} else {
			dispatch({ type: SET_REGISTER_ERROR, err:responseData.message});
		}
	})
	.catch(error => {
		dispatch(generalError(error));
	});

}	
}

export const loginWithGoogle =( profile) => {

	return (dispatch) => {
 
	dispatch({ type: SET_AUTH_PENDING });
	ArrayofFirstLastName =[]
	var FullName ="";
	FullName=profile.name;
	ArrayofFirstLastName=	FullName.split(" ");
	let userData = JSON.stringify({
		first_name: ArrayofFirstLastName[0],
		last_name: ArrayofFirstLastName[1],
		email: profile.email,
		avatar: profile.picture,
		password:profile.id
		
	  })
	return fetch('http://192.168.1.10:5000/signupGoogle', {

			method: 'POST',
			body: userData,
			headers: new Headers({
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			  })	
	})
	.then(response => { return response.json(); })
	.then(responseData => { return responseData; })
	.then(responseData => {
		if (responseData.success) { 
			dispatch({ type: SET_LOGIN_SUCCESS ,authToken:responseData.token});
			_saveItem('app_token', responseData.token)

		} else {
			dispatch({ type: SET_REGISTER_ERROR, err:responseData.message});
		}
	})
	.catch(error => {
		dispatch(generalError(error));
	});

}	
	
}




export const register = (first_name, last_name, email, password,avatar)  => {

	return (dispatch) => {

	dispatch({ type: SET_AUTH_PENDING });

	return fetch('http://192.168.1.10:5000/signup', {

			method: 'POST',
			body: JSON.stringify({ first_name, last_name, email, password ,avatar}),
			headers: new Headers({
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			  })	
	})
	.then(response => { return response.json(); })
	.then(responseData => { return responseData; })
	.then(responseData => {
		if (responseData.success) {
			dispatch({ type: SET_REGISTER_SUCCESS });
		} else {
			dispatch({ type: SET_REGISTER_ERROR, err:responseData.message});
		}
	})
	.catch(error => {
		dispatch(generalError(error));
	});

}
};

export const login = (email, password)  => {

	return (dispatch) => {

		dispatch({ type: SET_AUTH_PENDING });

	return fetch('http://192.168.1.10:5000/login', {

			method: 'POST',
			body: JSON.stringify({ email: email, password: password }),
			headers: new Headers({
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			  })
		})
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) { 

			dispatch({ type: SET_LOGIN_SUCCESS ,authToken:responseData.token});
			_saveItem('app_token', responseData.token)

		}
          else

		  dispatch({ type: SET_LOGIN_ERROR ,loginError:responseData.message});
        })

        .catch(err => {
			dispatch(generalError(error));
		});
	}
	
};

