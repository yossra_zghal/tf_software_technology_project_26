import {
  ADDING_OBJECT_ATTEMPT,
  ADDING_OBJECT_SUCCESS,
  ADDING_OBJECT_FAILED,
  LOADING_SUCCESS,LOADING_FAILED
} from "./types";
import axios from "axios";
import { AsyncStorage } from "react-native";

export function deleteObject(key) {
  console.log('from here delete'+key)
  return dispatch => {
    AsyncStorage.getItem("app_token").then(token => {
      fetch("http://192.168.1.10:5000/object/delete/" + key, {
        method: "DELETE",
        mode: "cors",
        headers: new Headers({
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
          //'Accept': 'application/json'
        })
      })
        .then(response => {
          return response.json();
        })
        //  .then(responseData => { return responseData; })
        .then(responseData => {
          if (responseData.success) {
            dispatch({ type: LOADING_SUCCESS, objects: responseData.objects });
          } else
            dispatch({ type: LOADING_FAILED, error: responseData.message });
        })

        .catch(err => {
          console.log("fetch delete object error" + err);
        });
    });
  };
}



export const addObject = ({ title, image, description }) => {
  return dispatch => {
    dispatch({ type: ADDING_OBJECT_ATTEMPT});

    let postData = JSON.stringify({
      title: title,
      description: description,
      image: image
    });

    AsyncStorage.getItem("app_token").then(token => {
      fetch("http://192.168.1.10:5000/object/create", {
        method: "POST",
        mode: "cors",
        body: postData,
        headers: new Headers({
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
          //'Accept': 'application/json'
        })
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          return responseData;
        })
        .then(responseData => {
          if (responseData.success) {
            dispatch({
              type: ADDING_OBJECT_SUCCESS,
              object: responseData.object
            });
          } else
            dispatch({
              type: ADDING_OBJECT_FAILED,
              error: responseData.message
            });
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    });
  };
};

export const alertUser = (data) => {
  return dispatch => {
    console.log("alert")
    dispatch({ type: ADDING_OBJECT_ATTEMPT});


    AsyncStorage.getItem("app_token").then(token => {
      fetch("http://192.168.1.10:5000/object/scan/"+data, {
        method: "GET",
        mode: "cors",
        headers: new Headers({
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        })
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          return responseData;
        })
        .then(responseData => {
          if (responseData.success) {
            dispatch({
              type: ADDING_OBJECT_SUCCESS,
              object: responseData.object
            });
          } else
            dispatch({
              type: ADDING_OBJECT_FAILED,
              error: responseData.message
            });
        })

        .catch(err => {
          console.log("fetch error" + err);
        });
    });
  };
};