import {
    LOADING_PROFILE_SUCCESS,
    LOADING_PROFILE_FAILED,
    LOADING_PROFILE,
    UPDATE_USER
    } from './types';
    
    import { AsyncStorage } from 'react-native';
    import axios from 'axios';
    
  


    export const fetchProfile = () => {
      return (dispatch) => {
        dispatch({ type: LOADING_PROFILE });
    
        AsyncStorage.getItem('app_token').then(token => {
  
          fetch("http://192.168.0.241:5000/user/profile", {
            method: "GET",
            mode: "cors",
            headers: new Headers({
              Authorization: "Bearer " + token,
              "Content-Type": "application/json",
              //'Accept': 'application/json'
            }),
          })
            .then((response) => {
              return response.json();
            })
            //  .then(responseData => { return responseData; })
            .then((responseData) => {
              if (responseData.success) {
                dispatch({
                  type: LOADING_PROFILE_SUCCESS,
                  user: responseData.user,
                });
              } else
                dispatch({
                  type: LOADING_PROFILE_FAILED,
                  error: responseData.message,
                });
            })

            .catch((err) => {
              console.log("fetch error fetchProfile " + err);
            });
        })
      
        
      }
    }

    export const getUser= (post) => {
      return (dispatch) => {

        dispatch({ type: LOADING_USER});
    
        AsyncStorage.getItem('app_token').then(token => {

          fetch('http://192.168.0.241:5000/user/find/'+post, {
            method: 'GET',
            mode: 'cors',
            headers: new Headers({
              "Authorization": 'Bearer ' + token,
              'Content-Type': 'application/json',
              //'Accept': 'application/json'
      
            })
      
          })
            .then(response => { return response.json(); })
          //  .then(responseData => { return responseData; })
            .then(responseData => {
              
              if (responseData.success) {  dispatch({ type: LOADING_USER_SUCCESS, userP: responseData.user }) }
              else
              dispatch({ type: LOADING_USER_FAILED, error: responseData.message })
            })
      
            .catch(err => {
              console.log(" GetUser  error" + err);
            });
        })
      }
    }


    export function editUser( first_name,last_name,bio,phone,dateOfbirth ) {
      return dispatch => {

        
        let postData = JSON.stringify({
          first_name: first_name,
          last_name: last_name,
          bio:bio,
          dateOfBirth:dateOfbirth,
          telephoneNumber:phone
        });
        AsyncStorage.getItem("app_token").then(token => {
          fetch("http://192.168.0.241:5000/user/update", {
            method: "PATCH",
            mode: "cors",
            headers: new Headers({
              Authorization: "Bearer " + token,
              "Content-Type": "application/json"
            }),
            body: postData
          })
            .then(response => {
              return response.json();
            })
            .then(responseData => {
              if (responseData.success) {
                dispatch({ type: UPDATE_USER, user: responseData.user });
              } 
            })
    
            .catch(err => {
              console.log("fetch error" + err);
            });
        });
      };
    }