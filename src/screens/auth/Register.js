import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Image,
  View,
  TouchableOpacity,
} from "react-native";

import { connect } from "react-redux";
import { reduxForm, Field } from "redux-form";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { LinearGradient } from "expo-linear-gradient";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import * as firebase from "firebase";
import ErrorBar from "../noUsedScreen/ErrorBar";
import { register } from "../../actions/auth.actions";
import { Ionicons } from "@expo/vector-icons";
import { Button, Block, Input, Text } from "../../components/logincomponents";
import { theme } from "../../constants/loginconstant";
import uuid from "uuid";
import {
  Alert,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
} from "react-native";
import { globalStyle, defaultNavigator } from "./style";
import { ScrollView } from "react-native-gesture-handler";

class Register extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    avatar: '',
	uploading: false,
	errors: [],
    loading: false
  
  };
  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);
  }
  
  componentDidUpdate(prevProps) {
	//9dim / actual jdida

	setTimeout(() => {
		if (!prevProps.registered && this.props.registered) {
			this.props.navigation.navigate("Login");
		  } 		  }, 2000);

  }
  _pickImage = async () => {
    //console.log('ghghghg')
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async (pickerResult) => {
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        var uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ avatar: uploadUrl });
        console.log(uploadUrl);
      }
    } catch (e) {
      console.log(e);
      alert("Upload failed, sorry 😞");
    } finally {
      this.setState({ uploading: false });
    }
  };

   renderInput = ({ input: { onChange, ...restInput }, placeholder }) => {
	const { loading, errors } = this.state;
	const hasErrors = key => (errors.includes(key) ? styles.hasErrors : null);
		return (
	  	<Input
		  email
	  	label={placeholder}
    	error={hasErrors(placeholder)}
        style={[styles.input, hasErrors(placeholder)]}
	//	placeholder={"entrez votre "+placeholder}
		onChangeText={onChange}
		{...restInput}
	  />
	);
  };
   renderPassword = ({ input: { onChange, ...restInput }, placeholder }) => {
	const { loading, errors } = this.state;
	const hasErrors = key => (errors.includes(key) ? styles.hasErrors : null);
	return (
	  <Input
	  	
		secure
		label={placeholder}
		//style={styles.input}
		error={hasErrors(placeholder)}
		style={[styles.input, hasErrors(placeholder)]}
		onChangeText={onChange}
		//placeholder={placeholder}
		{...restInput}
  	  />
	);
  };

  render() {
    const { navigation } = this.props;
    const { handleSubmit } = this.props;
    const submitForm = (e) => {

		const errors = [];
	
		Keyboard.dismiss();
		this.setState({ loading: true });
	
		console.log(this.state.errors)

		// check with backend API or with some static data
		if (e.first===undefined) errors.push("Firstname");
		if (e.last===undefined) errors.push("Lastname");
		if (e.email===undefined) errors.push("Email");
		if (e.password===undefined) errors.push("Password");

		this.setState({ errors, loading: false });
	
	
      this.props.register(
        e.first,
        e.last,
        e.email,
        e.password,
        this.state.avatar
      );
    };

    return (
      <KeyboardAwareScrollView  >
		  <ScrollView >
		  <View style={ styles.signup }>

        <Block padding={[0, theme.sizes.base * 2]}>
          <Text></Text>
          <Text></Text>
          <Text h1 bold>
            Sign Up
          </Text>
          <Block middle>
            <ErrorBar />
            <Text></Text>
            <TouchableOpacity
              style={styles.avatarPlaceholder}
              onPress={this._pickImage}
            >
              <Image
                source={{ uri: this.state.avatar }}
                style={styles.avatar}
              />
              <Ionicons
                name="ios-add"
                size={46}
                color="#FFF"
                style={{ marginTop: 6, marginLeft: 2 }}
              ></Ionicons>
            </TouchableOpacity>

            <Field
              name="first"
              placeholder="Firstname"
              component={this.renderInput}
            />
            <Field
              name="last"
              placeholder="Lastname"
              component={this.renderInput}
            />
            <Field name="email" placeholder="Email" component={this.renderInput} />
            <Field
              name="password"
              placeholder="Password"
              component={this.renderPassword}
            />
            <View style={styles.errorMessage}>
              <Text>{this.props.errorMessage}</Text>
            </View>

            <Button
              gradient
              onPress={handleSubmit(submitForm)}
              buttonStyle={[globalStyle.btn]}
              titleStyle={globalStyle.btnText}
            >
              <Text bold white center>
                Register
              </Text>
            </Button>
            <Button onPress={() => navigation.navigate("Login")}>
              <Text
                gray
                caption
                center
                style={{ textDecorationLine: "underline" }}
              >
                Back to Login
              </Text>
            </Button>
            {this.props.registered ? (
              <Text style={styles.loggedInDesc}>Register was successfull</Text>
            ) : null}
          </Block>
        </Block>
		</View>
		</ScrollView>
      </KeyboardAwareScrollView>
    );
  }
}



function mapStateToProps(store, ownProps) {
  return {
    errorMessage: store.auth.regError,
    registered: store.auth.registered,
    authToken: store.auth.authToken,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    register: (first, last, email, password, avatar) => {
      dispatch(register(first, last, email, password, avatar));
    },
  };
}
let RegisterConnect = connect(mapStateToProps, mapDispatchToProps)(Register);
export default reduxForm({
  form: "registerForm",
})(RegisterConnect);

async function uploadImageAsync(uri) {
  // Why are we using XMLHttpRequest? See:
  // https://github.com/expo/expo/issues/2402#issuecomment-443726662
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
      resolve(xhr.response);
    };
    xhr.onerror = function (e) {
      console.log(e);
      reject(new TypeError("Network request failed"));
    };
    xhr.responseType = "blob";
    xhr.open("GET", uri, true);
    xhr.send(null);
  });

  const ref = firebase.storage().ref().child(uuid.v4());
  const snapshot = await ref.put(blob);

  // We're done with the blob, close and release it
  blob.close();

  return await snapshot.ref.getDownloadURL();
}

const styles = StyleSheet.create({
	signup: {
		flex: 1,
		justifyContent: "center",
	  },
	  input: {
		borderRadius: 0,
		borderWidth: 0,
		borderBottomColor: theme.colors.gray2,
		borderBottomWidth: StyleSheet.hairlineWidth,
	  },
	  hasErrors: {
		borderBottomColor: theme.colors.accent,
	  },
  avatarPlaceholder: {
    width: 100,
    height: 100,
    backgroundColor: "#E1E2E6",
    borderRadius: 50,
    marginTop: 40,
    marginLeft: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    position: "absolute",
    width: 100,
    height: 100,
    marginLeft: 150,
  },
});
