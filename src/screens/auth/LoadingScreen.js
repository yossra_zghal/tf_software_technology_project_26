import React, { Component } from 'react';
import { StyleSheet, Text, View ,Alert,ActivityIndicator,AsyncStorage,Image } from 'react-native';


class LoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }



  componentDidMount(){

    this.checkIfLoggedIn();
  }
  checkIfLoggedIn = () => {
    AsyncStorage.getItem('app_token')
    .then(token => {
        if (token) {
          this._navigate('HomeApp');
        }else {
          this._navigate('Welcome');
        }
    });
  };

  _navigate(screen) {
    setTimeout(() => {
      this.props.navigation.navigate(screen);
    }, 2000 );

  }

  render() {
    return (
      <View style={styles.container}>
      <Image source={require('../../images/logofindit.jpg')} style={styles.image}/>
      <ActivityIndicator size={'small'} />
      <Text style={styles.loadingText}>Loading ...</Text>
    </View>
    );
  }
}

export default LoadingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(42, 55, 68)',
  },
  loadingText: {
    color: '#fff',
    fontSize: 20,
    paddingTop: 10
  },
  image: {
    width: 150,
    height: 150,
    marginBottom: 20
  }
});