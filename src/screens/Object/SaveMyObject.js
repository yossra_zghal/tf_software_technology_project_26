import React from "react";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import * as Print from "expo-print";
import { CustomButton } from "../../components/common/custom-button";
import { Block, Input, Text } from "../../components/logincomponents";
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  ScrollView,
  Picker,
  TextInput,
  AsyncStorage,
} from "react-native";
import { theme } from "../../constants/loginconstant";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import Modal from "react-native-modal";
import QRCode from "react-native-qrcode-svg";
import { Container, Content, Header, Form, Item, Label } from "native-base";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { RadioButton } from "react-native-paper";
import uuid from "uuid";
import * as firebase from "firebase";
import Constants from "expo-constants";
import { addObject } from "../../actions/object.actions";
import { Root, Toast, Popup } from "popup-ui";
import { connect } from "react-redux";
import { setArrayQrCode } from "../../actions/qrcode.actions";
import ImageUpload from '../Post/ImageUpload'

//console.disableYellowBox = true;

const url =
  "https://console.firebase.google.com/u/2/project/pfe-auth/storage/pfe-auth.appspot.com/files";

const firebaseConfig = {
  apiKey: "AIzaSyCDSo8s66lp3WK5q_O3O9lC8kV1zworLps",
  authDomain: "pfe-auth.firebaseapp.com",
  databaseURL: "https://pfe-auth.firebaseio.com",
  projectId: "pfe-auth",
  storageBucket: "pfe-auth.appspot.com",
  messagingSenderId: "518460442619",
  appId: "1:518460442619:web:50bc23ce9e254e249c6d5e",
  measurementId: "G-YWWK60K48S",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

class SaveMyObject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.callback = this.callback.bind(this);
  }

  state = {
    image: null,
    uploading: false,
    description: "",
    title: "",
    isModalVisible: false,
    visibleModal: null,
  };

  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);
  }
  _onAddPressed() {
    console.log("button pressed ");
    const { title, image, description } = this.state;
    this.props.addObject({ title, image, description });

    //this.setState({ visibleModal: 1 })
    //this.setState({ isModalVisible: !this.state.isModalVisible });
  }

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text>Hello!</Text>
      <QRCode
        size={150}
        value={this.props.savedObject ? this.props.savedObject._id : "Empty"}
        getRef={(c) => (this.svg = c)}
      />

      <Text style={{ marginTop: 5 }}>
        {this.props.savedObject ? this.props.savedObject._id : "Empty"}
      </Text>

      {this._renderButton("Close", () => this.getDataURL())}
    </View>
  );

  getDataURL = () => {
    this.svg.toDataURL(this.callback);
  };

  callback = (data) => {
    this.props.setArrayQrCode({ id: this.props.savedObject._id, image: data });
    this.setState({ visibleModal: null });
  };

  toggleModal = () => {
    this.setState({ visibleModal: 1 });
  };

  componentDidUpdate(prevProps) {
    //9dim / actual jdida
    if (!prevProps.savedObject && this.props.savedObject) {
      console.log(this.props.savedObject);

      this.toggleModal();
    }
  }

  render() {
    let { image } = this.state;

    return (
      <KeyboardAwareScrollView>
        <View style={styles.login}>
          <Block padding={[0, theme.sizes.base * 2]}>
            <Text h1 bold center>
              <Text></Text>
              Eassy way to{" "}
              <Text
                style={{
                  fontSize: 40,
                  color: "#12e3be",
                }}
              >
                Save
              </Text>{" "}
              Your Object ! Go ..
            </Text>
            <Block middle>
              <ScrollView>
                <Form style={styles.form}>
                  <View>
                    <View style={styles.SectionStyle}>
                      <Input
                        style={[styles.input]}
                        placeholder="Title"
                        value={this.state.title}
                        onChangeText={(title) => this.setState({ title })}
                      />
                    </View>

                    <View style={styles.SectionStyle2}>
                      <Input
                        style={[styles.input]}
                        underlineColorAndroid="transparent"
                        placeholder={"Description"}
                        placeholderTextColor={"#9E9E9E"}
                        value={this.state.description}
                        onChangeText={(description) =>
                          this.setState({ description })
                        }
                        numberOfLines={20}
                        multiline={true}
                      />
                    </View>

                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                     <ImageUpload
                        onImageUpload={(image) => this.setState({ image })}
                        image={this.state.image}
                      />
                    
                      <CustomButton
                        title="Save Object"
                        onPress={() => this._onAddPressed()}
                        style={styles.btn2}
                        textStyle={
                          {
                            /* styles for button title */
                          }
                        }
                      />

                      <Modal isVisible={this.state.visibleModal === 1}>
                        {this._renderModalContent()}
                      </Modal>
                    </View>
                  </View>
                </Form>

                {this.props.qrArr.map((item, index) => {
                  console.log(item.image);
                  return (
                    <View
                        keyExtractor={(item, index) => {
                return  index.toString();
               }} 
                 //     key={index}
                      style={{
                        marginVertical: 5,
                        alignItems: "center",
                        borderBottomColor: "rgba(0,0,0,0.5)",
                        borderBottomWidth: 0.4,
                      }}
                    >
                      <Image
                        source={{ uri: `data:image/svg;base64,${item.image}` }}
                        style={{ width: 200, height: 200 }}
                      />
                      <Text>{item.id}</Text>
                      <Button
                        title="print"
                        onPress={() => {
                          Print.printAsync({
                            html: `
          <html>
          <body>

          <div style="width: 1020px;height:1300px;border-radius: 30px; padding-top:1px; padding-bottom:1px;border: 1px solid  #FBEE27; text-align: center;background: #FBEE27;">
          <h1 style="font-size:10vw"> Chat Perdu</h1>
          <h1 style="font-size:5vw"> Mon petit chat de race siamoi perdu hier le 20/06/2020</h1>
         
         </Text>
         <center>
          <img src="https://images.unsplash.com/photo-1508927415581-538b97647924?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1489&q=80" width="700" height="500" />
          </center>
  
          <h1 style="font-size:5vw"> If you find it please call me : 52346552 </h1>
          <center>
          <div style="width: 200px;height:90px; padding-top:1px; padding-bottom:1px;border: 4px solid  red; text-align: center;background: #FBEE27;">
          <h1> reworded</h1>
          </div>
          </center>
          <br/>
          <br/>
          <h5>Please download To find application<img src="https://www.freelogodesign.org/file/app/client/thumb/935f6b37-2cd3-4bfb-937a-9c2b4e3c49ae_200x200.png?1592676806639" heght="100"width="100"/> Ready to find It !  </h5> 
          </div>
         

          </body>
          </html>
        `,
                          });
                        }}
                      />
                    </View>
                  );
                })}
              </ScrollView>
            </Block>
          </Block>
        </View>
      </KeyboardAwareScrollView>
    );
  }

  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: "rgba(0,0,0,0.4)",
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        >
          <ActivityIndicator color="#fff" animating size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { image } = this.state;
    if (!image) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: 150,
          borderRadius: 3,
          elevation: 2,
        }}
      >
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            shadowColor: "rgba(0,0,0,1)",
            shadowOpacity: 0.2,
            shadowOffset: { width: 4, height: 4 },
            shadowRadius: 5,
            overflow: "hidden",
          }}
        >
          <Image source={{ uri: image }} style={{ width: 150, height: 150 }} />
        </View>

       {/*  <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={{ paddingVertical: 10, paddingHorizontal: 10 }}
        >
          {image}
        </Text> */}
      </View>
    );
  };

 
}



const actions = { addObject, setArrayQrCode };


function mapStateToProps(state) {
  return {
    error: state.object.error,
    loading: state.object.loading,
    savedObject: state.object.object,
    qrArr: state.qrCode.qrArr,
  };
}

export default connect(mapStateToProps, actions)(SaveMyObject);

const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: "center",
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  button: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0,
  },
  valueText: {
    fontSize: 18,
    marginBottom: 50,
  },

  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center",
  },
  btn: {
    backgroundColor: "#abeee2",
  },
  btn2: {
    backgroundColor: "#12e3be",
    width: 400,
  },
});
