import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, Button, Alert } from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { alertUser } from "../../actions/object.actions";
import { connect } from "react-redux";
import {
  SCLAlert,
  SCLAlertButton
} from 'react-native-scl-alert'
import { FontAwesome ,AntDesign } from '@expo/vector-icons'; 



const QRScanner = (props) => {
  const [owner, setOwner] = useState(false);
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [object, setObject] = useState(props.object)
  useEffect(() => {
    setObject(props.object)
   // console.log(props.object) // bech tthabbek itha sarelha invocation ou non
  }, [props.object])

  
   useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    // Alert.alert(`hello data is ${data}`)
   props.alertUser(data);
   setTimeout(() => {
     console.log("hello",object)
    setOwner(true)
  }, 4000);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }
  
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-end",
      }}
    >
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      { object === null ?( 
         
          <SCLAlert
          show={owner}
          onRequestClose={()=>setOwner(false)}
          theme="danger"
          title="Edit"
          subtitle={`this QrCode does not belong to any user please verify !!  `}
          headerIconComponent={<FontAwesome name="exclamation-circle" size={32} color="white" />} >
          <SCLAlertButton theme="danger" onPress={()=>setOwner(false)}>Close</SCLAlertButton>
        </SCLAlert>) :( <SCLAlert
          show={owner}
          onRequestClose={()=>setOwner(false)}
          theme="info"
          title="Hello Finder"
          subtitle={object.author.telephoneNumber === undefined ? `You have found ${object.title} this belong to  ${object.author.first_name} ${object.author.last_name}`:`You have found ${object.title} this belong to  ${object.author.first_name} ${object.author.last_name}  Call Him/Her Phone :${object.author.telephoneNumber}`}
          headerIconComponent={<AntDesign name="find" size={32} color="white" />} >
          <SCLAlertButton theme="info"    onPress={() =>{
            setOwner(false)
           props.navigation.navigate("ShowUserProfile", {
              user: object.author,
            })
          }
          
                }>Done</SCLAlertButton>
        </SCLAlert>)
        
      }

      {scanned && (
        <Button title={"Tap to Scan Again"} onPress={() => setScanned(false)} />
      )}
    </View>
  );
};

const mapStateToProps = function (state) {
  //console.log(state.object.object);
  return {
    object: state.object.object,
  };
};
export const QRScan = connect(mapStateToProps, { alertUser })(QRScanner);
