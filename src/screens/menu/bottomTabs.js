import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  StyleSheet,
} from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { HomeStack } from "./HomeStack";
import Profile from "../profile/Profile";
import Add from "../Post/Add";
import { Notifications } from "expo";
import { addNotifcation ,joinServer } from "../../actions/notification.actions";
import { connect } from "react-redux";
import { registerForPushNotificationsAsync } from "../../service/Notifcation.service";
import { IMAGE } from "../../constants/Image";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ConversationListScreen } from '../chat/conversation-list.component';
import LoginSocket from '../../socketIO/LoginSocket'
import ListGift from "../gift/ListGift";
import { NotificationScreen } from '../notification/allNotification';
import ConfettiCannon from 'react-native-confetti-cannon';
import Modal from "react-native-modal";
import SharingScreen from "../noUsedScreen/SharingScreen";
import ExploreContainer from "../gift/ExploreContainer";
import profile from "../noUsedScreen/profile";
import notif from "../noUsedScreen/notif";
import Chat from "../noUsedScreen/Chat";



const Tab = createBottomTabNavigator();
const navOptionHandler = () => ({
  headerShown: false,
});
class bottomTabs extends Component {
  // _isMounted =  false;

  constructor(props) {
    super(props);
    this.state = {
      shoot: false,
      visibleModal: null,
    };
  }


  componentDidMount() {
    // this._isMounted = true;
    registerForPushNotificationsAsync();
    this.props.joinServer();
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  _renderModalContent = () => (
    <View style={styles.modalContent}>
       <View style={styles.card}>
          <Text style={styles.paragraphHeading}>Congrates !!</Text>
          <View style={{ backgroundColor: 'grey', width: '100%', height: 1 }} />
          <Text style={styles.paragraph}>You got a Cashback</Text>
          <Image
            style={styles.logo}
            source={{
              uri: 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/gift.png',
            }}
          />
          {/* <Text style={styles.textLarge}>$22.22</Text> */}
          <View
            style={{
              marginTop: 20,
              backgroundColor: 'green',
              width: '100%',
            }}>
            <Text
              style={{
                color: 'white',
                fontSize: 18,
                padding: 10,
                textAlign: 'center',
              }}
              onPress={() => {
                
                this.setState({ visibleModal: null })
                this.setState({ shoot: false });
this.props.navigation.navigate("Gift");
              }
              }>
              Get it now
            </Text>
          </View>
        </View>
        {this.state.shoot ? (
          <ConfettiCannon count={200} origin={{ x: -10, y: 0 }} />
        ) : null}
    </View>
  );
  

  _handleNotification = (notification) => {
    // do whatever you want to do with the notification
    this.setState({ notification: notification });
    if (notification.data.message !== undefined && notification.data.message.includes("Congratulations")) {
      
      setTimeout(() => {
        this.setState({ shoot: true });
        this.setState({ visibleModal: 1 });
            }, 10000);
   

    }
    this.props.addNotifcation(notification);
  };

  

  render() {
    return (
      <>
      <Tab.Navigator
     
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === "Home") {
              iconName = focused ? IMAGE.ICON_HOME : IMAGE.ICON_HOME_BLACK;
            } else if (route.name === "Settings") {
              iconName = focused
                ? IMAGE.ICON_SETTINGS
                : IMAGE.ICON_SETTINGS_BLACK;
            } else if (route.name === "Scan") {
              iconName = focused
                ? IMAGE.ICON_QR_CODE
                : IMAGE.ICON_QR_CODE_BLACK;
            }   else if (route.name === "Chat") {
              iconName = focused ? IMAGE.ICON_CHAT : IMAGE.ICON_CHAT_BLACK;
            }  else if (
              route.name === "Search"
            ) {
              iconName = focused ? IMAGE.ICON_SEARCH : IMAGE.ICON_SEARCH_BLACK;
            } else if (route.name === "Notification") {
              iconName = focused ? IMAGE.ICON_BELL : IMAGE.ICON_BELL_BLACK;
            } else if (route.name === "Profile") {
              iconName = focused ? IMAGE.ICON_USER : IMAGE.ICON_USER_BLACK;
            }

            // You can return any component that you like here!
            return (
              <Image
                source={iconName}
                style={{ width: 20, height: 20 }}
                resizeMode="contain"
              />
            );
          },
        })}
        tabBarOptions={{
          labelStyle: {
           fontWeight:"bold"
          },
          activeTintColor: "#1ba1f2",
          inactiveTintColor: "black",
          keyboardHidesTabBar: true,
        }}
      >
        <Tab.Screen name="Home" component={HomeStack}     />
        <Tab.Screen name="Chat" component={Chat}    />

        <Tab.Screen
          name="Add"
          component={Add}
   
          options={{
            tabBarLabel: "",
            tabBarIcon: ({ color, size }) => (
              <View
                style={{
                  position: "absolute",
                  alignSelf: "center",
                  backgroundColor: "grey",
                  width: 70,
                  height: 70,
                  borderRadius: 35,
                  bottom: -17,
                  //                zIndex: 10
                }}
              >
                <TouchableWithoutFeedback>
                  <View style={[styles.button, styles.actionBtn]}>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("Add")}
                    >
                      <Image
                        style={{ width: 60, height: 60 }}
                        resizeMode="contain"
                        source={require('../../images/FB.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            ),
          }}
          
        />

        <Tab.Screen name="Notification" component={notif} options={{headerMode: 'none', headerShown : false}} />
        <Tab.Screen name="Profile" component={Profile}  options={navOptionHandler}/>
      </Tab.Navigator>
      <Modal isVisible={this.state.visibleModal === 1}>
                        {this._renderModalContent()}
       </Modal>
    
      </>
    );
  }
}
/* function mapDispatchToProps(dispatch) {
	return {
    sendSocketIo:dispatch({ type: 'server/join' }) ,
    addNotifcation: addNotifcation()
		
	}; */

export default connect(null, {addNotifcation, joinServer})(bottomTabs);
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue",
  },
  button: {
    width: 60,
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "grey",
    shadowOpacity: 0.1,
    shadowOffset: { x: 2, y: 0 },
    shadowRadius: 2,
    borderRadius: 30,
    position: "absolute",
    bottom: 20,
    right: 0,
    top: 5,
    left: 5,
  },
  actionBtn: {
    backgroundColor: "#1E90FF",
    textShadowOffset: { width: 5, height: 5 },
    textShadowRadius: 10,
    borderWidth: 2,
    borderColor: "#fff",
  },card: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    paddingTop : -5,
    backgroundColor: 'white',
  },
  textLarge: {
    margin: 24,
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'green',
  },
  paragraphHeading: {
    margin: 10,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'green',
  },
  paragraph: {
    margin: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  logo: {
    height: 84,
    width: 84,
  },
  modalContent: {
    backgroundColor: "white",
    // padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  valueText: {
    fontSize: 18,
    marginBottom: 50,
  },


});
