import React, { useState, useEffect } from "react";
import { ScrollView, View, TextInput } from "react-native";
import {
  Button,
  Layout,
  StyleService,
  Text,
  useStyleSheet,  Icon,
  Datepicker,Divider,Input
} from "@ui-kitten/components";
import { ProfileSetting } from "./extra/profile-setting.component";
import { ProfileAvatar } from "./extra/profile-avatar.component";
import { CameraIcon } from "./extra/icons";
import { Profile } from "./extra/data";
import { connect } from "react-redux";
import { editUser } from "../../../actions/user.actions";
import Inputs from "../../../components/Input";


const profile: Profile = Profile.jenniferGreen();
const DateIcon = (style) => (
  <Icon {...style} name='calendar'/>
) 
const PhoneIcon = (style) => (
  <Icon {...style} name='phone'/>
) 
const SettingProfile = (props): React.ReactElement => {
  //const { style, hint, value, ...layoutProps } = props;
  const renderHintElement = (hint): React.ReactElement => (
    <Text appearance="hint" category="s1">
      {hint}
    </Text>
  );
  const [currentUser, setCurrentUser] = useState(props.user);

  const [email, setEmail] = useState(props.user.email);
  const [firstName, setFirstName] = useState(props.user.first_name);
  const [lastName, setLastName] = useState(props.user.last_name);
  const [avatar, setAvatar] = useState(props.user.avatar);
  const [date, setDate ] = useState(new Date())
  const [phoneNumber, setPhoneNumber] = useState(props.user.telephoneNumber);
  const [bio, setBio] = useState(props.user.bio);

  useEffect(() => {
    setCurrentUser(props.user);
  }, [props.user]); //[props.user] 2eme parametre reste en ecoute si il y'a un change pour l'affecter au setCureentUser

  const styles = useStyleSheet(themedStyles);

  const onDoneButtonPress = (): void => {
    props.navigation && props.navigation.goBack();
  };

  const onSubmit =():void => {
    props.editUser(firstName,lastName,bio,phoneNumber,date);
    props.navigation.navigate("Profile")
  }


  const renderPhotoButton = (): React.ReactElement => (
    
    <Button
      style={styles.photoButton}
      size="small"
      status="basic"
      icon={CameraIcon}
    />
  );

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
    >

      <Layout style={styles.photoSection} level="1">
        <ProfileAvatar
          style={styles.photo}
          source={{uri:avatar}}
          editButton={renderPhotoButton}
        />
        <View style={styles.nameSection}>
          <React.Fragment>
            <Layout level="1" style={[styles.setting, styles.containerSetting]}>
              <TextInput 
              defaultValue={firstName}       
              
              onChangeText={text => setFirstName(text)}
              
              
              />
            </Layout>
            <Divider />
            <Layout level="1" style={[styles.setting, styles.containerSetting]}>
            <TextInput 
            
            
            defaultValue={lastName}       
              
            onChangeText={text => setLastName(text)}
            
            
            />
            </Layout>
            <Divider />
          </React.Fragment>
        </View>
      </Layout>
      <Layout
          level="1"
          style={[styles.setting, styles.containerSetting,styles.description]}
        >
        <Text>Bio</Text>
 
      <TextInput   placeholder='You Can talk about yourSelf Here' 
                 defaultValue={bio}       
              
                 onChangeText={text => setBio(text)}
                  />
        </Layout>

{/*       <Text style={styles.description} appearance="hint">
        {profile.description}
      </Text> */}
      <React.Fragment>
        <Layout
          level="1"
          style={[styles.setting, styles.containerSetting, styles.emailSetting]}
        >
          {"Email" && renderHintElement("Email")}
          <Text category="s1">{email}</Text>
        </Layout>
        <Divider />
      </React.Fragment>

      <React.Fragment>
        <Layout
          level="1"
          style={[styles.setting, styles.containerSetting]}
        >
          {"Date Of Birth" && renderHintElement("Date Of Birth")}
          <Datepicker
         icon={DateIcon}
          label=''
          date={date}
          onSelect={setDate}
        />
        </Layout>
        <Divider />
      </React.Fragment>
      <React.Fragment>
        <Layout
          level="1"
          style={[styles.setting, styles.containerSetting]}
        >
          {"Phone Number" && renderHintElement("Phone Number")}
          <Input
                  placeholder='Phone      '
                  icon={PhoneIcon}
                  value={phoneNumber}
                  onChangeText={setPhoneNumber}
                />       
                
                 </Layout>
        <Divider />
      </React.Fragment>
 
      <Button style={styles.doneButton} onPress={onSubmit}>
        DONE
      </Button>
    </ScrollView>
  );
};
const mapStateToProps = function (state) {
  return {
    user: state.user.user,
  };
};
export const ProfileSettings = connect(mapStateToProps, {editUser })(
  SettingProfile
);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: "background-basic-color-2",
  },
  contentContainer: {
    paddingBottom: 24,
  },
  photoSection: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
  },
  photo: {
    aspectRatio: 1.0,
    height: 76,
  },
  photoButton: {
    aspectRatio: 1.0,
    height: 32,
    borderRadius: 16,
  },
  nameSection: {
    flex: 1,
    marginHorizontal: 8,
  },
  description: {
    padding: 24,
    backgroundColor: "background-basic-color-1",
  },
  doneButton: {
    marginHorizontal: 24,
    marginTop: 24,
  },
  setting: {
    padding: 16,
  },
  emailSetting: {
    marginTop: 24,
  },
  containerSetting: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  input: {
    borderWidth: 0,
    //  borderBottomColor: theme.colors.gray2,
  },
});
