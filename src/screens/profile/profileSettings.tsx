import React from 'react';
import { StyleSheet } from 'react-native';
import { TopNavigation, TopNavigationAction } from '@ui-kitten/components';
import { SafeAreaLayout } from '../chat/messanger/extra/safe-area-layout.component';
import { ArrowIosBackIcon } from '../chat/messanger/extra/icons-chat';
import  { ProfileSettings } from './profile-settings-2/index';

export const ProfileSettingsScreen = ({ navigation }): React.ReactElement => {

  const renderBackAction = (): React.ReactElement => (
    <TopNavigationAction
      icon={ArrowIosBackIcon}
      onPress={navigation.goBack}
    />
  );

  return (
    <SafeAreaLayout
      style={styles.container}
      insets='top'>
      <TopNavigation
        title='Profile'
        leftControl={renderBackAction()}
      />
      <ProfileSettings navigation={navigation}/>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
