import React, { Component } from "react";
import { connect } from "react-redux";
import { Rating } from "react-native-elements";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  ImageBackground,
  Platform,
  View,
  FlatList,
} from "react-native";
import { SCLAlert, SCLAlertButton } from "react-native-scl-alert";
import { Block, Text, theme } from "galio-framework";
import { Button } from "../../components";
import { Images, argonTheme } from "../../constants";
import { HeaderHeight } from "../../constants/utils";
import { TouchableOpacity } from "react-native-gesture-handler";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { fetchGrade } from "../../actions/grade.action";
import {  notifyOffline } from "../../actions/notification.actions";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;
class NavigateProfile extends Component {
  
  constructor(props) {
    super(props);
    //
  }
  state = {
    postList: [],
    startingValue: 0,
    show: false,
    gradeList: [],
    countComment: 0,
  };
  componentDidMount() {
    const { navigation } = this.props;

    navigation.addListener("focus", () => {
      this.props.fetchGrade();
    });
    this.setState({
      postList: this.props.posts.filter(
        (post) => post.author._id === this.props.route.params.user._id
      ),
    });

    for (const post of this.props.posts) {
      for (const comment of post.comments) {
        if (comment.postedBy._id === this.props.route.params.user._id)
          this.setState((prevState) => {
            return {
              countComment: prevState.countComment + 1,
            };
          });
      }
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.grades !== this.props.grades) {
      this.setState({
        gradeList: this.props.grades,
      });
      setTimeout(() => this.calculateGrade(), 1000);
    }
  }

  calculateGrade = () => {
    let { user } = this.props.route.params;
    const { lowPoint, highPoint } = this.state.gradeList.find(
      (grade) => grade._id === user.grade
    );
    var score = user.score;
    var calcul;
    switch (user.grade) {
      case 4:
        calcul = (score - lowPoint) / (highPoint - lowPoint) + 4;
        break;
      case 3:
        calcul = (score - lowPoint) / (highPoint - lowPoint) + 3;
        break;
      case 2:
        calcul = (score - lowPoint) / (highPoint - lowPoint) + 2;
        break;
      case 1:
        calcul = (score - lowPoint) / (highPoint - lowPoint) + 1;
        break;
      case 0:
        calcul = (score - lowPoint) / (highPoint - lowPoint);
        break;
      default:
    }
    console.log(calcul);
    this.setState({ startingValue: calcul });
  };
  handleClose = () => {
    this.setState({ show: false });
  };
  handleOpen = () => {
    this.setState({ show: true });
  };

  render() {
    const { startingValue } = this.state;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <View>
            <Block flex style={styles.profile}>
              <Block flex>
                <ImageBackground
                  source={Images.ProfileBackground}
                  style={styles.profileContainer}
                  imageStyle={styles.profileBackground}
                >
                  <Block flex style={styles.profileCard}>
                    <Block middle style={styles.avatarContainer}>
                      <Image
                        source={{ uri: this.props.route.params.user.avatar }}
                        style={styles.avatar}
                      />
                    </Block>
                    <Block style={styles.info}>
                      <Block
                        middle
                        row
                        space="evenly"
                        style={{ marginTop: 20, paddingBottom: 24 }}
                      >
                        <Button
                          small
                          style={{
                            backgroundColor: argonTheme.COLORS.DEFAULT,
                          }}
                          onPress={() => {
                         {this.props.usersOnline.find( user => user.userId ===  this.props.route.params.user._id) === undefined ?
                           setTimeout(
                             () => this.setState({ show: true }),
                             500
                           ) :
                           this.props.navigation.navigate("Conversation", {
                            name: `${this.props.route.params.user.first_name} ${this.props.route.params.user.last_name} `,
                            userId: this.props.route.params.user._id,
                          }) 

                          }
                         
                          }}
                        >
                          MESSAGE
                        </Button>
                      </Block>
                      <Block row space="between">
                        <Block middle>
                          <Text
                            bold
                            color="#525F7F"
                            size={18}
                            style={{ marginBottom: 4 }}
                          >
                            {this.state.postList.length}
                          </Text>
                          <Text size={12} color={argonTheme.COLORS.TEXT}>
                            Posts
                          </Text>
                        </Block>
                        <Block middle>
                          <Text
                            bold
                            color="#525F7F"
                            size={18}
                            style={{ marginBottom: 4 }}
                          >
                            {this.state.countComment}
                          </Text>
                          <Text size={12} color={argonTheme.COLORS.TEXT}>
                            Comments
                          </Text>
                        </Block>
                      </Block>
                    </Block>
                    <Block flex>
                      <Block middle style={styles.nameInfo}>
                        <Text bold size={28} color="#32325D">
                          {this.props.route.params.user.first_name}{" "}
                          {this.props.route.params.user.last_name}
                        </Text>

                        <Text
                          size={16}
                          color="#525F7F"
                          style={{ textAlign: "center" }}
                        >
                          {this.props.route.params.user.bio}
                        </Text>

                        <TouchableOpacity
                          // onPress={() => this.setState({ isVisible: true })}
                          activeOpacity={1}
                        >
                          <Rating
                            showRating
                            readonly
                            type="star"
                            fractions={2}
                            startingValue={startingValue}
                            imageSize={20}
                            onFinishRating={this.ratingCompleted}
                            style={{ paddingVertical: 10 }}
                          />
                        </TouchableOpacity>
                        <SCLAlert
                          show={this.state.show}
                          onRequestClose={this.handleClose}
                          theme="warning"
                          title="Info"
                          subtitle={
                            this.props.route.params.user.first_name +
                            " " +
                            this.props.route.params.user.last_name +
                            " is offline now but you can notify him "
                          }
                          headerIconComponent={
                            <MaterialCommunityIcons
                              name="bell-ring"
                              size={32}
                              color="white"
                            />
                          }
                        >
                          <SCLAlertButton
                            theme="warning"
                            onPress={() => {
                              this.props.notifyOffline(
                                this.props.route.params.user._id
                              )
                              this.setState({ show: false });

                            }
                            }
                          >
                            Notify Now
                          </SCLAlertButton>
                          <SCLAlertButton
                            theme="default"
                            onPress={this.handleClose}
                          >
                            Cancel
                          </SCLAlertButton>
                        </SCLAlert>
                      </Block>
                      <Block
                        middle
                        style={{ marginTop: 30, marginBottom: 16 }}
                      ></Block>
                      <Block style={styles.divider} />

                      <Block
                        row
                        style={{
                          paddingVertical: 14,
                          alignItems: "baseline",
                        }}
                      >
                        <Text bold size={16} color="#525F7F">
                          Posts
                        </Text>
                      </Block>
                      <View>
                        {this.props.loading ? (
                          <Text>Loading Please Wait</Text>
                        ) : (
                          <FlatList
                            data={this.state.postList}
                            keyExtractor={(item, index) => {
                              return index.toString();
                            }}
                            showsVerticalScrollIndicator={false}
                            numColumns={3}
                            renderItem={({ item, index }) => {
                              return (
                                <Block
                                  key={`post${index}`}
                                  style={{
                                    paddingBottom: -HeaderHeight * 2,
                                  }}
                                >
                                  <Block
                                    row
                                    space="between"
                                    style={{ flexWrap: "wrap" }}
                                  >
                                    <TouchableOpacity
                                      onPress={() =>
                                        this.props.navigation.navigate(
                                          "Article",
                                          {
                                            post: item,
                                          }
                                        )
                                      }
                                    >
                                      <Image
                                        source={{ uri: item.image }}
                                        style={{
                                          margin: 1,
                                          height:
                                            Dimensions.get("window").width /
                                            3.5,
                                          width:
                                            Dimensions.get("window").width /
                                            3.5,
                                          resizeMode: "cover",
                                        }}
                                      />
                                    </TouchableOpacity>
                                  </Block>
                                </Block>
                              );
                            }}
                          />
                        )}
                      </View>
                    </Block>
                  </Block>
                </ImageBackground>
              </Block>
            </Block>
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  profile: {
    // marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    // width: width,
    // height: height,
    //padding: 0,
    //zIndex: 1
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: "relative",
    marginTop: -80,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: 10,
  },
  divider: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#E9ECEF",
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: "center",
    width: thumbMeasure,
    height: thumbMeasure,
  },
});

const mapStateToProps = (state) => {
  return {
    posts: state.postsList.posts,
    grades: state.grades.grades,
    usersOnline :state.Chat.usersOnline
  };
};

export default connect(mapStateToProps, {fetchGrade ,notifyOffline })(NavigateProfile);
