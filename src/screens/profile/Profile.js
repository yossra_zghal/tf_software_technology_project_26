import React from "react";
import { connect } from "react-redux";
import { fetchProfile } from "../../actions/user.actions";
import { fetchObjects } from "../../actions/ObjectList.action";
import { fetchPosts, deletePost, editPost } from "../../actions/post.actions";
import { deleteObject } from "../../actions/object.actions";
import * as Icon from "@expo/vector-icons";
import { YellowBox, SafeAreaView } from "react-native";
import jwtDecode from "jwt-decode";

import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  ImageBackground,
  Platform,
  View,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import { Block, Text, theme } from "galio-framework";
import StepModal from "react-native-step-modal";
import { Button } from "../../components";
import { Images, argonTheme } from "../../constants";
import { HeaderHeight } from "../../constants/utils";
import { Rating } from "react-native-elements";
import { fetchGrade } from "../../actions/grade.action";
import ListGrade from "./ListGrade";
import { SCLAlert, SCLAlertButton } from "react-native-scl-alert";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import { MaterialCommunityIcons,FontAwesome } from '@expo/vector-icons'; 
import Input from "../../components/Input";
import SwitchSelector from 'react-native-switch-selector';
import _ from 'lodash';

const { width, height } = Dimensions.get("screen");
YellowBox.ignoreWarnings([
  "VirtualizedLists should never be nested", // TODO: Remove when fixed
]);
const _console = _.clone(console);
console.warn = message => {
if (message.indexOf('VirtualizedLists should never be nested') <= -1) {
 _console.warn(message);
} 
};
const thumbMeasure = (width - 48 - 32) / 3;
const options = ["Cancel", "Edit", "Delete"];

const optionSwitch = [
  { label: 'Free', value: false },
  { label: 'Reward ', value: true }
 
];
class Profile extends React.Component {
  constructor(props) {
    super(props);

    this._onRefreshTasks = this._onRefreshTasks.bind(this);
  }
  state = {
    startingValue: 0,
    gradeName: "Grade :",
    gradeList: [],
    show: false,
    user: this.props.user,
    postList:  this.props.posts.filter((post) => AsyncStorage.getItem("app_token").then((jwt) => {jwtDecode(jwt)._id === post.author._id}))
      ,
    objectList: [],
    countComment: 0,
    showDeletePost: false,
    showEditPost: false,
    title: "",
    description: "",
    reward:false
  };

  componentDidUpdate(_, prevState) {
    if (this.props.posts !== prevState.postList) {

      AsyncStorage.getItem("app_token").then((jwt) => { 

        var decode = jwtDecode(jwt);

        this.setState({
          postList: this.props.posts.filter(
            (post) => post.author._id === decode._id
          ),
        })
      })
    }
  }
  componentDidMount() {
    const { navigation } = this.props;
    // navigation.addListener("focus", () => {
    this.setState({ user: this.props.user });
    // this.props.fetchObjects();
    this.setState({ objectList: this.props.objects });
    AsyncStorage.getItem("app_token").then((jwt) => {
      var decode = jwtDecode(jwt);
      this.setState({
        postList: this.props.posts.filter(
          (post) => post.author._id === decode._id
        ),
      }); 

      for (const post of this.props.posts) {
        for (const comment of post.comments) {
          if (comment.postedBy._id === decode._id)
            this.setState((prevState) => {
              return {
                countComment: prevState.countComment + 1,
              };
            });
        }
      }
    });

    setTimeout(() => this.calculateGrade(), 1000);
  }
  handleClose = () => {
    this.setState({ show: false });
  };
  handlerLongClick = () => {
    this.setState({ show: true });
  };
  showActionSheet = () => {
    this.ActionSheet.show();
  };
  _onRefreshTasks() {
    this.props.fetchPosts();
    this.props.fetchProfile();
    this.props.fetchObjects();
  }
  handleDelete = (item) => {
    this.props.deleteObject(item._id);
    this._onRefreshTasks();
    this.setState({ show: false });
  };
  renderModel = (post) => {
    return (
      <>
        <SCLAlert
          show={this.state.showDeletePost}
          onRequestClose={this.handleClose}
          theme="danger"
          title="Info"
          subtitle="You can setup the colors using the theme prop"
          headerIconComponent={
            <MaterialCommunityIcons name="delete" size={32} color="white" />
          }
        >
          <SCLAlertButton
            theme="danger"
            onPress={() => this.handleDeletePost(post)}
          >
            Done
          </SCLAlertButton>
          <SCLAlertButton theme="default" onPress={this.handleClose}>
            Cancel
          </SCLAlertButton>
        </SCLAlert>

        <SCLAlert
          show={this.state.showEditPost}
          onRequestClose={this.handleClose}
          theme="warning"
          title="Edit"
          subtitle="You can Edit your Post"
          headerIconComponent={
            <FontAwesome name="edit" size={32} color="white" />
          }
        >
          <Input
            style={[styles.input]}
            placeholder="Title"
            defaultValue={post.title}
            onChangeText={(title) => this.setState({ title })}
          />
          <Input
            style={[styles.input]}
            placeholder="Description"
            defaultValue={post.description}
            onChangeText={(description) => this.setState({ description })}
          />

          <SwitchSelector
            options={optionSwitch}
            initial={post.reward ? 1 : 0}
            onPress={(value) => this.setState({ reward: value })}
          />
          <SCLAlertButton theme="warning" onPress={() => this.handleEdit(post)}>
            Done
          </SCLAlertButton>
        </SCLAlert>
      </>
    );
  };

  renderAction = () => {
    return (
      <ActionSheet
        ref={(o) => (this.ActionSheet = o)}
        title={
          <Text style={{ color: "#000", fontSize: 18 }}>Choose Option ?</Text>
        }
        options={options}
        cancelButtonIndex={0}
        destructiveButtonIndex={4}
        onPress={(index) => {
          if (index == 2)
            setTimeout(() => this.setState({ showDeletePost: true }), 500);
          else if (index == 1)
            setTimeout(() => this.setState({ showEditPost: true }), 500);
        }}
      />
    );
  };
  handleDeletePost = (post) => {
    this.props.deletePost(post._id);
    // this._onRefreshTasks();
    this.setState({ showDeletePost: false });
  };
  handleEdit = (post) => {
    var id = post._id;
    const { title, description, reward } = this.state;
    this.props.editPost(title, description, reward, post);
    this._onRefreshTasks();
    this.setState({ showEditPost: false });
  };

  calculateGrade = () => {
    let { user } = this.state;
    var score = user.score;
    var calcul;
    switch (user.grade._id) {
      case 4:
        calcul =
          (score - user.grade.lowPoint) /
            (user.grade.highPoint - user.grade.lowPoint) +
          4;
        break;
      case 3:
        calcul =
          (score - user.grade.lowPoint) /
            (user.grade.highPoint - user.grade.lowPoint) +
          3;
        break;
      case 2:
        calcul =
          (score - user.grade.lowPoint) /
            (user.grade.highPoint - user.grade.lowPoint) +
          2;
        break;
      case 1:
        calcul =
          (score - user.grade.lowPoint) /
            (user.grade.highPoint - user.grade.lowPoint) +
          1;
        break;
      case 0:
        calcul =
          (score - user.grade.lowPoint) /
          (user.grade.highPoint - user.grade.lowPoint);
        break;
      default:
    }
    this.setState({ startingValue: calcul });
    this.setState({ gradeName: "Grade :" + user.grade.gradeName });
  };

  render() {
    let Component1 = (
      <View>
        <ListGrade
          score={this.props.user.score}
          gradeUser={this.props.user.grade}
        />
      </View>
    );

    return (
      <ScrollView showsVerticalScrollIndicator={false}>
      
        <View>
          {this.props.user == null ? (
            <Text>Loading Please Wait</Text>
          ) : (
            <View>
              <Block flex style={styles.profile}>
                <Block flex>
                  <ImageBackground
                    source={Images.ProfileBackground}
                    style={styles.profileContainer}
                    imageStyle={styles.profileBackground}
                  >
                    <Block flex style={styles.profileCard}>
                      <Block middle style={styles.avatarContainer}>
                        <Image
                          source={{ uri: this.state.user.avatar }}
                          style={styles.avatar}
                        />
                      </Block>
                      <Block style={styles.info}>
                        <Block
                          middle
                          row
                          space="evenly"
                          style={{ marginTop: 20, paddingBottom: 24 }}
                        >
                          <Button
                            small
                            style={{
                              backgroundColor: argonTheme.COLORS.DEFAULT,
                            }}
                          >
                           PROFILE
                          </Button>
                          <TouchableOpacity
                            onPress={() =>
                              this.props.navigation.navigate("ProfileSetting")
                            }
                          >
                            <Icon.Feather size={20} name="settings" />
                          </TouchableOpacity>
                        </Block>
                        <Block row space="between">
                          {/* <Block middle>
                            <Text
                              bold
                              size={18}
                              color="#525F7F"
                              style={{ marginBottom: 4 }}
                            >
                              {this.state.objectList !== undefined
                                ? this.state.objectList.length
                                : 0}
                            </Text>
                            <Text size={12} color={argonTheme.COLORS.TEXT}>
                              Objects
                            </Text>
                          </Block> */}
                          <Block middle>
                            <Text
                              bold
                              color="#525F7F"
                              size={18}
                              style={{ marginBottom: 4 }}
                            >
                              {this.state.postList.length}
                            </Text>
                            <Text size={12} color={argonTheme.COLORS.TEXT}>
                              Posts
                            </Text>
                          </Block>
                          <Block middle>
                            <Text
                              bold
                              color="#525F7F"
                              size={18}
                              style={{ marginBottom: 4 }}
                            >
                              {this.state.countComment}
                            </Text>
                            <Text size={12} color={argonTheme.COLORS.TEXT}>
                              Comments
                            </Text>
                          </Block>
                        </Block>
                      </Block>
                      <Block flex>
                        <Block middle style={styles.nameInfo}>
                          <Text bold size={28} color="#32325D">
                            {this.state.user.first_name}{" "}
                            {this.state.user.last_name}
                          </Text>

                          {/* <Text
                              size={16}
                              color="#525F7F"
                              style={{ textAlign: "center" }}
                            >
                              {this.props.user.bio}
                            </Text> */}

                          <TouchableOpacity
                            onPress={() => this.setState({ isVisible: true })}
                            activeOpacity={1}
                          >
                            <Rating
                              showRating
                              readonly
                              type="star"
                              fractions={2}
                              startingValue={this.state.startingValue}
                              imageSize={20}
                              onFinishRating={this.ratingCompleted}
                              style={{ paddingVertical: 10 }}
                            />
                          </TouchableOpacity>
                          <StepModal
                            switchVisibility={this.state.isVisible}
                            stepComponents={[Component1]}
                            onVisibilityChange={(isVisible) =>
                              this.setState({ isVisible })
                            }
                          />
                        </Block>
                              <Block
                          middle
                          style={{ marginTop: 30, marginBottom: 16 }}
                        >
                      </Block> 
                        <Block style={styles.divider} />

                        <Block
                          row
                          style={{
                            paddingVertical: 14,
                            alignItems: "baseline",
                          }}
                        >
                          <Text bold size={16} color="#525F7F">
                            Posts
                          </Text>
                        </Block>
                        <View>
                          {this.props.loading ? (
                            <Text>Loading Please Wait</Text>
                          ) : (

                            <FlatList
                              data={this.state.postList}
                              keyExtractor={(item, index) => {
                                return index.toString();
                              }}
                              showsVerticalScrollIndicator={false}
                              numColumns={3}
                              renderItem={({ item, index }) => {
                                return (
                                  <Block
                                    key={`post${index}`}
                                    style={{
                                      paddingBottom: -HeaderHeight * 2,
                                    }}
                                  >
                                    <Block
                                      row
                                      space="between"
                                      style={{ flexWrap: "wrap" }}
                                    >
                                      <TouchableOpacity
                                        onPress={() =>
                                          this.props.navigation.navigate(
                                            "Article",
                                            {
                                              post: item,
                                              connectedUser: this.props.user
                                                ._id,
                                            }
                                          )
                                        }
                                        onLongPress={this.showActionSheet}
                                      >
                                        <Image
                                          source={{ uri: item.image }}
                                          style={{
                                            margin: 1,
                                            height:
                                              Dimensions.get("window").width /
                                              3.5,
                                            width:
                                              Dimensions.get("window").width /
                                              3.5,
                                            resizeMode: "cover",
                                          }}
                                        />
                                      </TouchableOpacity>
                                      {this.renderModel(item)}
                                    </Block>
                                  </Block>
                                );
                              }}
                            />
                          )}
                        </View>
                        {/* <Block
                          row
                          style={{
                            paddingVertical: 14,
                            alignItems: "baseline",
                          }}
                        >
                          <Text bold size={16} color="#525F7F">
                            Objects
                          </Text>
                        </Block> */}
                        <View>
                          {this.props.loading ? (
                            <Text>Loading Please Wait</Text>
                          ) : (
                            <FlatList
                              data={this.props.objects}
                              keyExtractor={(item, index) => {
                                return index.toString();
                              }}
                              showsVerticalScrollIndicator={false}
                              numColumns={3}
                              renderItem={({ item, index }) => {
                                return (
                                  <Block
                                    style={{
                                      paddingBottom: -HeaderHeight * 2,
                                    }}
                                    key={`object${index}`}
                                  >
                                    <Block
                                      row
                                      space="between"
                                      style={{ flexWrap: "wrap" }}
                                    >
                                      <TouchableOpacity
                                        onPress={() =>
                                          this.props.navigation.navigate(
                                            "ObjectDetails",
                                            {
                                              object: item,
                                            }
                                          )
                                        }
                                        onLongPress={this.handlerLongClick}
                                      >
                                        <Image
                                          source={{ uri: item.image }}
                                          style={{
                                            margin: 1,
                                            height:
                                              Dimensions.get("window").width /
                                              3.5,
                                            width:
                                              Dimensions.get("window").width /
                                              3.5,
                                            resizeMode: "cover",
                                          }}
                                        />
                                      </TouchableOpacity>
                                      <SCLAlert
                                        show={this.state.show}
                                        onRequestClose={this.handleClose}
                                        theme="danger"
                                        title="Delete "
                                        subtitle="Are you sure to delete this ?"
                                        headerIconComponent={
                                          <MaterialCommunityIcons
                                            name="delete"
                                            size={32}
                                            color="white"
                                          />
                                        }
                                      >
                                        <SCLAlertButton
                                          theme="danger"
                                          onPress={() =>
                                            this.handleDelete(item)
                                          }
                                        >
                                          Done
                                        </SCLAlertButton>
                                        <SCLAlertButton
                                          theme="default"
                                          onPress={this.handleClose}
                                        >
                                          Cancel
                                        </SCLAlertButton>
                                      </SCLAlert>
                                    </Block>
                                    {this.renderAction()}
                                  </Block>
                                );
                              }}
                            />
                          )}
                        </View> 
                      </Block>
                    </Block>
                  </ImageBackground>
                </Block>
              </Block>
            </View>
          )}
        </View>
       </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  profile: {
    // marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    // width: width,
    // height: height,
    //padding: 0,
    //zIndex: 1
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: "relative",
    marginTop: -80,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: 10,
  },
  divider: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#E9ECEF",
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: "center",
    width: thumbMeasure,
    height: thumbMeasure,
  },
});

const mapStateToProps = (state) => {
  return {
    loading: state.user.loading,
    user: state.user.user,
    posts: state.postsList.posts,
    objects: state.objects.objects,
  };
};

export default connect(mapStateToProps, {
  fetchProfile,
  fetchPosts,
  deletePost,
  editPost,
  fetchObjects,
  fetchGrade,
  deleteObject,
})(Profile);
