import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  DrawerContentComponentProps,
  DrawerContentOptions,
  DrawerContentScrollView,
  DrawerItem,
} from "@react-navigation/drawer";
import { useSelector } from "react-redux";
import { connect } from "react-redux";

import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  AsyncStorage,
} from "react-native";
import {
  Avatar,
  Caption,
  Drawer,
  Paragraph,
  Switch,
  Text,
  Title,
  TouchableRipple,
  useTheme,
} from "react-native-paper";
import Animated from "react-native-reanimated";
import { logout } from "../../actions/auth.actions";

import { PreferencesContext } from "./preferencesContext";

type Props = DrawerContentComponentProps<DrawerNavigationProp>;

function DrawerContent(props: Props) {
  const profile = useSelector((state) => state.user.user);
  const paperTheme = useTheme();
  const { theme, toggleTheme } = React.useContext(PreferencesContext);



  const translateX = Animated.interpolate(props.progress, {
    inputRange: [0, 0.5, 0.7, 0.8, 1],
    outputRange: [-100, -85, -70, -45, 0],
  });

  return (
    <DrawerContentScrollView {...props}>
      <Animated.View
        //@ts-ignore
        style={[
          styles.drawerContent,
          {
            backgroundColor: paperTheme.colors.surface,
            transform: [{ translateX }],
          },
        ]}
      >
        <View style={styles.userInfoSection}>
          <TouchableOpacity
            style={{ marginLeft: 10 }}
            onPress={() => {
              props.navigation.toggleDrawer();
            }}
          >
            <Avatar.Image
              source={{
                uri: profile.avatar,
              }}
              size={50}
            />
          </TouchableOpacity>
          <Title style={styles.title}>
            {profile.first_name} {profile.last_name}
          </Title>
          <Caption style={styles.caption}>{profile.email}</Caption>
          <View style={styles.row}>
            <View style={{ marginTop: -1, flexDirection: "row" }}>
              <Image
                source={require("../../../assets/icons/rating.png")}
                style={{ width: 30, height: 30 }}
              />
            </View>
            <View style={styles.section}>
              <Caption style={styles.caption}>Level </Caption>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                {profile.grade !== undefined ? (
                  <Text>{profile.grade.gradeName}</Text>
                ) : (
                  <Text>Please Wait</Text>
                )}
              </Paragraph>
            </View>
            <View style={styles.section}>
              <Caption style={styles.caption}>Score </Caption>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                {profile.score}
              </Paragraph>
            </View>
          </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          <DrawerItem
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="account-outline"
                color={color}
                size={size}
              />
            )}
            label="Profile"
            onPress={() => {props.navigation.navigate("Profile")}}
          />

          <DrawerItem
            icon={({ color, size }) => (
              <MaterialCommunityIcons
                name="wallet-giftcard"
                color={color}
                size={size}
              />
            )}
            label="Gift Card"
            onPress={() => {props.navigation.navigate("GiftList")}}
          /> 
          <DrawerItem
          icon={({ color, size }) => (
            <MaterialCommunityIcons
              name="bookmark-outline"
              color={color}
              size={size}
            />
          )}
          label="Bookmarks"
          onPress={() => {props.navigation.navigate("BookmarksPost")}}
        />
          <DrawerItem
            icon={({ color, size }) => (
              <MaterialCommunityIcons name="logout" color={color} size={size} />
            )}
            label="LogOut"
            onPress={()=> {
              props.logoutButton(props) 
              props.navigation.navigate("Login");

            
            }}

          />
        </Drawer.Section>
        <Drawer.Section title="Preferences">
          <TouchableRipple onPress={toggleTheme}>
            <View style={styles.preference}>
              <Text>Dark Theme</Text>
              <View pointerEvents="none">
                <Switch value={theme === "dark"} />
              </View>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </Animated.View>
    </DrawerContentScrollView>
  );
}
function mapDispatchToProps(dispatch) {
  return {
    logoutButton: () => {
      dispatch(logout());
    },
  };
}

export default connect(null, mapDispatchToProps)(DrawerContent);
const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    marginTop: 20,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
    marginTop: 7,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
