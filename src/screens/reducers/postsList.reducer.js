import {
  LOADING_FAILED,
  LOADING_SUCCESS,
  LOADING_POSTS,
  POST_LIKE,
  POST_DISLIKE,
  ADDING_COMMMENT,
  DELETE_COMMMENT,
} from "../actions/types";
const INITIAL_STATE = { loading: false, error: "", posts: [] };
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOADING_POSTS:
      return { ...state, loading: true };
    case LOADING_SUCCESS:
      return { ...state, loading: false, posts: action.posts };
    case LOADING_FAILED:
      return { ...state, loading: false, error: action.error };
    case POST_LIKE:
      let connectedUserId = action.connectedUserId;
      return {
        ...state,
        posts: state.posts.map((post) => {
          if (post._id === action.postId) {
            return {
              ...post,
              likes: [...post.likes, connectedUserId],
              likes_number: post.likes_number + 1,
            };
          } else {
            return post;
          }
        }),
      };
    case POST_DISLIKE:
      let connectedUser = action.connectedUser;
      var filteredArray = [];
      return {
        ...state,
        posts: state.posts.map((post) => {
          if (post._id === action.postId) {
            filteredArray = post.likes.filter(
              (item) => item !== connectedUser
            );

            return {
              ...post,
              likes: filteredArray,
              likes_number: post.likes_number - 1,
            };
          } else {
            return post;
          }
        }),
      };
   case ADDING_COMMMENT:   
      let commantaire = action.comment;
      return {
        ...state,
        posts: state.posts.map((post) => {
          if (post._id === action.postId) {
            return {
              ...post,
              comments: [...post.comments, commantaire],
              comments_number: post.comments_number + 1,
            };
          } else {
            return post;
          }
        }),
      };
    case DELETE_COMMMENT:
      var filtredComments = [];
      var deletedCommentId = action.commentId;
      return {
        ...state,
        posts: state.posts.map((post) => {
          if (post._id === action.postId) {
            filtredComments = post.comments.filter(
              (item) => item._id !== deletedCommentId
            );
            return {
              ...post,
              comments: filtredComments,
              comments_number: filtredComments.length,
            };
          } else {
            return post;
          }
        }),
      };

    default:
      return state;
  }
};
