
import {
  ADDING_OBJECT_ATTEMPT,
    ADDING_OBJECT_SUCCESS,
    ADDING_OBJECT_FAILED
  } from '../actions/types';
  import { combineReducers } from 'redux';

  const INITIAL_STATE = { loading: false, error: '', object: null }
  
  
  
  export default(state = INITIAL_STATE, action) => {
    switch(action.type) {
      case ADDING_OBJECT_ATTEMPT:
        return { ...INITIAL_STATE, loading: true }
      case ADDING_OBJECT_SUCCESS:
        return { ...INITIAL_STATE, loading: false, object: action.object }
      case ADDING_OBJECT_FAILED:
        return { ...INITIAL_STATE, loading: false, error: action.error }
      default:
        return state;
    }
  }



