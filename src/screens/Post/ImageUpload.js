import React, { Component } from "react";
import { Button } from "react-native-elements";
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";



import * as ImagePicker from "expo-image-picker";
import uuid from "uuid";
import * as firebase from "firebase";
import * as Permissions from "expo-permissions";

const firebaseConfig = {
  apiKey: "AIzaSyCDSo8s66lp3WK5q_O3O9lC8kV1zworLps",
  authDomain: "pfe-auth.firebaseapp.com",
  databaseURL: "https://pfe-auth.firebaseio.com",
  projectId: "pfe-auth",
  storageBucket: "pfe-auth.appspot.com",
  messagingSenderId: "518460442619",
  appId: "1:518460442619:web:50bc23ce9e254e249c6d5e",
  measurementId: "G-YWWK60K48S",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
export default class ImageUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
  }
  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);
  }
  render() {
    let { image } = this.props;
    return (
      <View>
        <View
          style={{
            justifyContent: "space-between",
            padding: 10,
          }}
        >
          <Button onPress={this._pickImage} title="Pick From Gallery" />
        </View>
        <View
          style={{
            justifyContent: "space-between",
            padding: 10,
          }}
        >
          <Button
            style={{
              justifyContent: "space-between",
              padding: 10,
            }}
            onPress={this._takePhoto}
            title="Take a photo"
          />
          {this._maybeRenderImage()}
          {this._maybeRenderUploadingOverlay()}
        </View>
      </View>
    );
  }

  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: "rgba(0,0,0,0.4)",
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        >
          <ActivityIndicator color="#fff" animating size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { image } = this.props;
    if (!image) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: 150,
          borderRadius: 3,
          elevation: 2,
        }}
      >
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            shadowColor: "rgba(0,0,0,1)",
            shadowOpacity: 0.2,
            shadowOffset: { width: 4, height: 4 },
            shadowRadius: 5,
            overflow: "hidden",
          }}
        >
          <Image source={{ uri: image }} style={{ width: 150, height: 150 }} />
        </View>
      </View>
    );
  };

  _takePhoto = async () => {
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _pickImage = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async (pickerResult) => {
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        var uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
        console.log(uploadUrl);
        this.props.onImageUpload(uploadUrl);
      }
    } catch (e) {
      console.log(e);
      alert("Upload failed, sorry :(");
    } finally {
      this.setState({ uploading: false });
    }
  };
}

async function uploadImageAsync(uri) {
  // Why are we using XMLHttpRequest? See:
  // https://github.com/expo/expo/issues/2402#issuecomment-443726662
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
      resolve(xhr.response);
    };
    xhr.onerror = function (e) {
      console.log(e);
      reject(new TypeError("Network request failed"));
    };
    xhr.responseType = "blob";
    xhr.open("GET", uri, true);
    xhr.send(null);
  });

  const ref = firebase.storage().ref().child(uuid.v4());
  const snapshot = await ref.put(blob);

  // We're done with the blob, close and release it
  blob.close();

  return await snapshot.ref.getDownloadURL();
}
const styles = StyleSheet.create({
  btn: {
    backgroundColor: "red",
  },
});
