import { View, Text, Image, StyleSheet } from "react-native";
import { HeaderHeight } from "../../constants/utils";
export const colors = {
  darkHl: "#bec6ce",
  text: "#fff",
};
export const gs = StyleSheet.create({
    sectionTitle:{
        fontWeight:"700",
        color:colors.text,
        fontStyle:'normal'

    },
	
  absoluteFull: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
  },
  smallText: {
    fontSize: 12,
    fontWeight: "800",
    color: colors.text,
  },
});
