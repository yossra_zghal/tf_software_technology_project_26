import React, { Component } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  StyleSheet,
  Image,
  Alert,
  TextInput
} from "react-native";
import { fetchPosts, deletePost ,addReclamation,editPost} from "../../actions/post.actions";
import { ListItem, Spinner } from "../../components/common";
import { Ionicons } from "@expo/vector-icons";
import { SimpleLineIcons } from "@expo/vector-icons";
import { TagSelect } from "react-native-tag-select";
import Input from "../../components/Input";
import { Button } from "../../components";
import { argonTheme } from "../../constants";
import { connect } from "react-redux";
import moment from "moment";
import GestureRecognizer from "react-native-swipe-gestures";
import { fetchProfile } from "../../actions/user.actions";
import { like, dislike, sendMessage } from "../../actions/comment.like.actions";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import Modal from "react-native-modal";
import {
  SCLAlert,
  SCLAlertButton
} from 'react-native-scl-alert'
import { MaterialCommunityIcons,AntDesign,FontAwesome } from '@expo/vector-icons'; 
import SwitchSelector from 'react-native-switch-selector';
import jwtDecode from "jwt-decode";
import colors from "../../components/components/styles/colors";
import HeartButton from '../../components/components/buttons/HeartButton'
import { TouchableHighlight } from "react-native-gesture-handler";
import {fetchObjects} from '../../actions/ObjectList.action'


/* const [userId,setUserId]=useState("")
AsyncStorage.getItem("app_token").then((jwt) => {
  var decode =jwtDecode(jwt)
  setUserId(decode._id)

 })  */


const options = ["Cancel", "Edit", "Delete"];
const options2 = ["Cancel", "Signalize"];
const data = [
  { id: 1, label: "#undesirable content" },
  { id: 2, label: "#Fake profile" },
  { id: 3, label: "#Violence" },
  { id: 4, label: "#Not Real " },
  { id: 5, label: "#Steal" },
];

const optionSwitch = [
  { label: 'Free', value: false },
  { label: 'Reward ', value: true }
 
];
class PostScreen extends Component {
  state = {
    message: "",
    mess: "",
    showMessages: false,
    showlikes: false,
    modalAction: false,
    notification: {},
    visibleModal: null,
    postsFromStore: this.props.posts,
    show: false,
    showEdit:false,
    title: "",
    description: "",
    reward:false
  };

  componentDidMount() {
    this.props.fetchPosts();
    this.props.fetchProfile();
    this.props.fetchObjects();

  }

  componentDidUpdate(prevProps) {
    if (prevProps.posts !== this.props.posts) {
      this.setState({
        postsFromStore: this.props.posts,
      });
    }
    if (!prevProps.saved && this.props.saved) {
      this.setState({ visibleModal: null });
      this.state.mess = "";
    }
  }
  _onRefreshTasks() {
    this.props.fetchPosts();
  }
  handleOpen = () => {
    this.setState({ show: true })
  }
  handleDelete = (post) => {
    this.props.deletePost(post._id)
    this._onRefreshTasks();
    this.setState({ show: false })
  }
  handleEdit = (post) => {
    var id=post._id;
    const { title, description, reward} = this.state;
    this.props.editPost(title, description ,reward, post);
    this._onRefreshTasks();
    this.setState({ showEdit: false })
  }
  handleClose = () => {
    this.setState({ show: false })
    this.setState({ showEdit: false })

  }


  toggleModal = () => {
    setTimeout(() => this.setState({ visibleModal: 1 }), 1000);

    console.log("here");
  };
  onSwipeUp(gestureState) {
    //this.props.navigation.navigate("ScanQr");
  }
  showActionSheet = () => {
    this.ActionSheet.show();
  };
  showActionSheet2 = () => {
    this.ActionSheet2.show();
  };
  functionOne(post, value) {

    const idpost = post._id;
    const { mess, keyword } = this.state;
    this.props.addReclamation({ idpost, mess, value });
  }
  _GoToProfile() {
  }
  _renderModalContent = (post) => (
    <View style={styles.modalContent}>
      <TouchableOpacity
        style={styles.touchable}
        onPress={() => this.setState({ visibleModal: null })}
      >
        <View style={styles.button}>
          <SimpleLineIcons name="close" size={18} color="red" />
        </View>
      </TouchableOpacity>
      <TagSelect
        data={data}
        max={3}
        ref={(tag) => {
          this.tag = tag;
        }}
        onMaxError={() => {
          Alert.alert("Ops", "Max ");
        }}
      />
      <View></View>
      <View style={styles.textAreaContainer}>
        <TextInput
          style={styles.textArea}
          multiline={true}
          placeholder="Write  Message !"
          numberOfLines={4}
          onChangeText={(mess) => this.setState({ mess })}
          value={this.state.mess}
        />

        <TouchableOpacity
          onPress={() => {
            this.functionOne(post, JSON.stringify(this.tag.itemsSelected));
          }}
        >
          <View style={styles.button}>
            <MaterialIcons name="send" size={24} color="black" />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );

  like = (post) => {
    console.log(" Mcha lel like");
    this.props.like(post._id);
  };

  dislike = (post) => {
    console.log(" Mcha lel dislike");
    this.props.dislike(post._id);
  };

  renderAction = (author, user) => {
    if (user == author)
      return (
        <ActionSheet
          ref={(o) => (this.ActionSheet = o)}
          title={
            <Text style={{ color: "#000", fontSize: 18 }}>Choose Option ?</Text>
          }
          options={options}

          
          cancelButtonIndex={0}
          destructiveButtonIndex={4}
        //  onPress={this.handlePress2}    this.handleOpen
        onPress={(index) => {
          console.log(index);
          if (index == 2)
            setTimeout(() => this.setState({ show: true }), 500);
            else  if (index == 1)
            setTimeout(() => this.setState({ showEdit: true }), 500);
        }}
        />
      );
    else {
      return (
        <ActionSheet
          ref={(o) => (this.ActionSheet2 = o)}
          title={
            <Text style={{ color: "#000", fontSize: 18 }}>Choose Option ?</Text>
          }
          options={options2}
          cancelButtonIndex={0}
          destructiveButtonIndex={4}
          onPress={(index) => {
            console.log(index);
            if (index == 1)
              setTimeout(() => this.setState({ visibleModal: 1 }), 500);
          }}
        />
      );
    }
  };

  renderHeart = (post) => {

    return (
      <TouchableOpacity
        onPress={() =>
          post.likes.indexOf(this.props.user._id) == -1
            ? this.like(post)
            : this.dislike(post)
        }
        style={{ paddingTop:13}}
      >
        <View>
          <Ionicons
            name="ios-heart"
            size={28}
            color="red"
            color={
              post.likes.indexOf(this.props.user._id) == -1 ? "#737888" : "red"
            }
          />
        </View>
      </TouchableOpacity>
    );
  };

  renderIcon = (reward) => {
    if (reward == true) {
      return (
        <View>
          <Ionicons name="ios-gift" size={20} color="#F22B53" />
        </View>
      );
    }
  };
  renderIA = (author, user) => {
    if (user == author) {
      return (
        <Ionicons
          name="ios-more"
          size={24}
          color="#737888"
          onPress={this.showActionSheet}
        />
      );
    } else
      return (
        <Ionicons
          name="ios-more"
          size={24}
          color="#737888"
          onPress={this.showActionSheet2}
        />
      );
  };

  renderPost = (post) => {
    return (
   
      <View style={styles.feedItem}>
        <Image source={{ uri: post.author.avatar }} style={styles.avatar} />
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <View>
          
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("ShowUserProfile", {
                    user: post.author,
                  })
                }
              >
                <Text style={styles.name}>
                  {post.author.first_name} {post.author.last_name}
                </Text>
                <Text style={styles.timestamp}>
                  {moment(post.createdAt).fromNow()}
                </Text>
              </TouchableOpacity>
            </View>
            {this.renderIcon(post.reward)}
            {this.renderIA(this.props.user._id, post.author._id)}
          </View>
          {this.renderAction(this.props.user._id, post.author._id)}

          <Text style={styles.post}>{post.title}</Text>
          <Text style={styles.post}>{post.description}</Text>
        
          <TouchableHighlight 
          activeOpacity={0.8}
      onPress={() => this.props.navigation.navigate( "Article", { post: post } )}> 
  <React.Fragment>
 {/*  <View style={styles.addToFavoriteBtn}>
              <HeartButton
                color={colors.white}
                selectedColor={colors.pink}
                // selected={favouriteListings.indexOf(listing.id) > -1}
                // onPress={() => handleAddToFav(listing)}
              />
            </View> */}
    <View>
            <Image
            source={{ uri: post.image }}
            style={styles.postImage}
            resizeMode="cover"
          /></View>

</React.Fragment>
</TouchableHighlight>
          <View style={{ flexDirection: "row" }}>
             {this.renderHeart(post)} 
            <Input
              onChange={this.onWriteComment.bind(this)}
              styles={styles.writeComment}
              value={this.state.message}
              placeholder="Write a message..."
            />
            {this.renderSendMessage(post)}
          </View>

          <View style={{ alignContent: "center" }}>
            {this.renderShowMessages(post)}
            <SCLAlert
          show={this.state.show}
          onRequestClose={this.handleClose}
          theme="danger"
          title="Info"
          subtitle="You can setup the colors using the theme prop"
          headerIconComponent={<MaterialCommunityIcons name="delete" size={32} color="white" />}
        >
          <SCLAlertButton theme="danger" onPress={()=>this.handleDelete(post)}>Done</SCLAlertButton>
          <SCLAlertButton theme="default" onPress={this.handleClose}>Cancel</SCLAlertButton>
        </SCLAlert>

        <SCLAlert
          show={this.state.showEdit}
          onRequestClose={this.handleClose}
          theme="warning"
          title="Edit"
          subtitle="You can Edit your Post"
          headerIconComponent={<FontAwesome name="edit" size={32} color="white" />}
        >
          <Input   style={[styles.input]}
                  placeholder="Title"
                  defaultValue={post.title}
                  onChangeText={(title) => this.setState({ title })}

                />
                  <Input   
                  style={[styles.input]}
                  placeholder="Description"
                  defaultValue={post.description}
                  onChangeText={(description) => this.setState({ description })}

                />
                
     <SwitchSelector options={optionSwitch} initial={post.reward?1:0} onPress={value =>  this.setState({ reward:value })} />
          <SCLAlertButton theme="warning" onPress={()=>this.handleEdit(post)}>Done</SCLAlertButton>
          {/* <SCLAlertButton theme="default" onPress={this.handleClose}>Cancel</SCLAlertButton> */}
        </SCLAlert>
            <Modal isVisible={this.state.visibleModal === 1}>
              {this._renderModalContent(post)}
            </Modal>
          </View>
        </View>
      </View>
      
    );
  };

  showMessages = () => {
    this.setState({
      showMessages: !this.state.showMessages,
    });
  };
  renderMessages = (post) => {
    if (post !== undefined) {
      if (this.state.showMessages) {
        return post.comments.map((comment, i) => {
          return (
            <View style={styles.container2}>
              <TouchableOpacity onPress={() => {}}>
                <Image
                  style={styles.image}
                  source={{ uri: comment.postedBy.avatar }}
                />
              </TouchableOpacity>
              <View style={styles.content}>
                <View style={styles.contentHeader}>
                  <Text style={styles.name2}>
                    {comment.postedBy.first_name} {comment.postedBy.last_name}
                  </Text>
                  <Text style={styles.time}>
                    {moment(comment.created).fromNow()}
                  </Text>
                </View>
                <Text rkType="primary3 mediumLine"> {comment.text}</Text>
                <View style={styles.separator} />
              </View>
            </View>
          );
        });
      }
    }
  };
  _listEmptyComponent ()  {
    return (
      <View style={{flex: 1,backgroundColor: '#fff',justifyContent: "center",  alignItems: "center"}}>  
      <Image source={require("../../images/emptyArticle.gif")} />
        </View>
    )
  }
  
  onSendComment = (post) => {
    this.props.sendMessage(post._id, this.state.message);
    this.setState({
      message: "",
    });
  };
  renderSendMessage = (post) => {
    if (this.state.message.length > 0) {
      return (
        <View style={{ paddingTop: 16, paddingLeft: 10 }}>
          <Button
            onPress={() => this.onSendComment(post)}
            small
            style={{ backgroundColor: argonTheme.COLORS.INFO }}
          >
            Send
          </Button>
        </View>
      );
    }
  };

  onWriteComment = (text) => {
    this.setState({
      message: text.nativeEvent.text,
    });
  };

  renderShowMessages = (post) => {
    return (
      
      <View style={{ flexDirection: "row" }}>
       
        <TouchableOpacity  onPress={() =>
        this.props.navigation.navigate(
          "PostDetail",
          {
            post: post,
          }
        )
      }
          //   onPress={this.showlikes.bind(this)}
          style={{ marginLeft: 2 }}
          activeOpacity={1}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.textSeeComments}>
              {post.likes_number} likes |
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() =>
        this.props.navigation.navigate(
          "PostDetail",
          {
            post: post,
          }
        )
      }
          // onPress={this.showMessages.bind(this)}
          style={{ marginLeft: 8 }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.textSeeComments}>
              {post.comments_number} comments
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80,
    };
    return (
     
        <View style={styles.container}>
          {this.props.loading ? (
  <View style={{flex: 1,backgroundColor: '#fff',justifyContent: "center",  alignItems: "center" ,}}>  
  <Image source={require("../../images/emptyArticle.gif")} />
    </View>          ) : (
            <FlatList
              style={styles.feed}
              data={this.state.postsFromStore}
              renderItem={({ item }) => this.renderPost(item)}
              keyExtractor={(item, index) => {
                return index.toString();
              }}
              // ListEmptyComponent={this._listEmptyComponent()}

              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.loading}
                  onRefresh={this._onRefreshTasks.bind(this)}
                />
              }
            />
          )} 
        </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#EFECF4",
  },
  header: {
    paddingTop: 64,
    paddingBottom: 16,
    backgroundColor: "#FFF",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#EBECF4",
    shadowColor: "#454D65",
    shadowOffset: { height: 5 },
    shadowRadius: 15,
    shadowOpacity: 0.2,
    zIndex: 10,
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: "500",
  },
  feed: {
    marginHorizontal: 16,
  },
  feedItem: {
    backgroundColor: "#FFF",
    borderRadius: 5,
    padding: 8,
    flexDirection: "row",
    marginVertical: 8,
  },
  avatar: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginRight: 16,
  },
  name: {
    fontSize: 15,
    fontWeight: "500",
    color: "#454D65",
  },
  timestamp: {
    fontSize: 11,
    color: "#C4C6CE",
    marginTop: 4,
  },
  post: {
    marginTop: 16,
    fontSize: 14,
    color: "#838899",
  },
  load: {
    paddingTop: 20,
    color: "#C4C6CE",
    fontSize: 11,
  },
  postImage: {
    width: undefined,
    height: 150,
    borderRadius: 5,
    marginVertical: 16,
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },  addToFavoriteBtn: {
  
    position: 'absolute',
    right: 14,
    top: 20,
    zIndex: 2,
  }
});
const mapStateToProps = (state) => {
  return {
    error: state.postsList.error,
    loading: state.postsList.loading,
    posts: state.postsList.posts,
    user: state.user.user,
    saved: state.reclamation.saved,
  };
};

export default connect(mapStateToProps, {
  fetchPosts,
  fetchProfile,
  fetchObjects,
  deletePost,
  sendMessage,
  like,
  addReclamation,
  dislike,
  editPost
})(PostScreen);
