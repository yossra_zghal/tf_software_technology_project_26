import React, { useState, useEffect } from "react";
import {
  ScrollView,
  View,
  TextInput,
  TouchableWithoutFeedback,
} from "react-native";
import {
  Button,
  Layout,
  StyleService,
  Text, 
  useStyleSheet,
  Datepicker,
  Divider,
  Input,
  Select,Toggle,CheckBox 
} from "@ui-kitten/components";
import  { city }  from './city'
import { connect } from "react-redux";
import SwitchSelector from "react-native-switch-selector";
import  * as Icon from '@expo/vector-icons';

const PostAdding = ({setAddedPost}): React.ReactElement => {
  const optionSwitch = [
    {
      label: "  Lost",
      value: false,
      imageIcon: require("../../images/lost.png"),
    },
    {
      label: "Found ",
      value: true,
      imageIcon: require("../../images/found.png"),
    },
  ];
  const data = [
    { text: "Human" , value: 0 },
    { text: "Pets" , value: 1 },
    { text: "Item" , value: 2 },
  ];
  const desrciptionIcon = () =>   <Icon.MaterialIcons key={'desc'}
//   style={{ marginRight: 10 }}
  name="description"
  size={20}
/>
 const titleIcon = () =>   <Icon.MaterialIcons
 key={'titl'}
//  style={{ marginRight: 10 }}
 name="title"
 size={20}
/>
  const renderHintElement = (hint): React.ReactElement => (
    <Text appearance="hint" category="s1">
      {hint}
    </Text>
  );
  const styles = useStyleSheet(themedStyles);
  const [title, setTitle] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [lostFound, setLostFound] = React.useState(false);
  const [selectedOption, setSelectedOption] = React.useState("");
  const [selectedCity, setSelectedCity] = React.useState("");
  const [reward, setReward] = React.useState(false);
  const onCheckedChange = (isChecked) => {
    setReward(isChecked);
  };

  React.useEffect(() => {
    setAddedPost({
        title,
        description,
        reward,
        category: selectedOption.value,
        city: selectedCity.value,
        type: lostFound===true?'found':'lost'
    })
  }, [title, description, lostFound, selectedOption, selectedCity, reward])

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
    >
      <Layout style={styles.photoSection} level="1">
        <View style={styles.nameSection}>
          <Layout level="1" style={[styles.setting]}>
            {"Title" && renderHintElement("Title")}
            <Input
              value={title}
              placeholder="entre le titre du publication"
              onChangeText={(nextValue) => setTitle(nextValue)}
            //   icon={titleIcon}

            />
          </Layout>
          <Divider />

          <Layout level="1" style={[styles.setting]}>
            {"Description" && renderHintElement("Description")}
            <Input
              multiline={true}
              textStyle={{ minHeight: 70 }}
              placeholder="entre le descirption"
              onChangeText={(nextValue) => setDescription(nextValue)}
            //   icon={desrciptionIcon}

            />
            <Divider />
          </Layout>

          <SwitchSelector
            options={optionSwitch}
            initial={0}
            onPress={(value) => setLostFound(value)}
          />
                      <Divider />

          <Layout level="1" style={[styles.setting]}>
            {"Category" && renderHintElement("Category")}
            <Select
            data={data}
            selectedOption={selectedOption}
            onSelect={(value)=>setSelectedOption(value)}
            />
          </Layout>
          <Divider />
          <Layout level="1" style={[styles.setting]}>
            {"City" && renderHintElement("City")}
            <Select
            data={city}
            selectedOption={selectedCity}
            onSelect={(value)=>setSelectedCity(value)}
            />
          </Layout>
          <Divider />

      <Layout level="1" style={[styles.setting]}>
            {"Reward" && renderHintElement("Reward")}
      <Toggle
      style ={{ paddingRight:170}}
      checked={reward}
      onChange={onCheckedChange}
      
    />         
     </Layout>
        </View>
      </Layout> 
    </ScrollView>
  );
};
const mapStateToProps = function (state) {
  return {
    user: state.user.user,
  };
};
export const AddPostForm = connect(mapStateToProps, null)(PostAdding);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: "background-basic-color-2",
  },
  contentContainer: {
    paddingBottom: 24,
  },
  photoSection: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
  },
  photo: {
    aspectRatio: 1.0,
    height: 76,
  },
  photoButton: {
    aspectRatio: 1.0,
    height: 32,
    borderRadius: 16,
  },
  nameSection: {
    flex: 1,
    marginHorizontal: 8,
  },
  description: {
    padding: 24,
    backgroundColor: "background-basic-color-1",
  },
  doneButton: {
    marginHorizontal: 24,
    marginTop: 24,
  },
  setting: {
    //  padding: 16,
  },
  emailSetting: {
    marginTop: 24,
  },
  containerSetting: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  input: {
    borderWidth: 0,
    //  borderBottomColor: theme.colors.gray2,
  },
});
