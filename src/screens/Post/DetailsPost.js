import React, { Component } from 'react'
import { Text, StyleSheet, View, Animated, Image, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
import { deletePost, editPost } from "../../actions/post.actions";
import { connect } from "react-redux";
import { SCLAlert, SCLAlertButton } from "react-native-scl-alert";

  import Swipeable from 'react-native-swipeable';
  import { deleteComment } from "../../actions/comment.like.actions";
  import moment from "moment";
  import { Ionicons } from "@expo/vector-icons";

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import * as theme from './theme';
import Adresse from './Adresse'

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  flex: {
    flex: 0,
  },
  column: {
	  
    flexDirection: 'column'
  },
  row: {
    flexDirection: 'row'
  },
  header: {
    // backgroundColor: 'transparent',
    paddingHorizontal: theme.sizes.padding,
    paddingTop: theme.sizes.padding,
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  back: {
    width: theme.sizes.base * 3,
    height: theme.sizes.base * 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  content: {
    // backgroundColor: theme.colors.active,
    // borderTopLeftRadius: theme.sizes.border,
    // borderTopRightRadius: theme.sizes.border,
  },
  contentHeader: {
    backgroundColor: 'transparent',
    padding: theme.sizes.padding,
    backgroundColor: theme.colors.white,
    borderTopLeftRadius: theme.sizes.radius,
    borderTopRightRadius: theme.sizes.radius,
    marginTop: -theme.sizes.padding / 2,
  },
  avatar: {
    position: 'absolute',
    top: -theme.sizes.margin,
    right: theme.sizes.margin,
    width: theme.sizes.padding * 2,
    height: theme.sizes.padding * 2,
    borderRadius: theme.sizes.padding,
  },
  shadow: {
    shadowColor: theme.colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  dotsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 36,
    right: 0,
    left: 0
  },
  dots: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 6,
    backgroundColor: theme.colors.gray,
  },
  title: {
    fontSize: theme.sizes.font * 2,
    fontWeight: 'bold'
  },
  description: {
    fontSize: theme.sizes.font * 1.2,
    lineHeight: theme.sizes.font * 2,
    color: theme.colors.caption
  },container2: {
    paddingLeft: 19,
    paddingRight: 16,
    paddingVertical: 12,
    flexDirection: "row",
    alignItems: "flex-start",
  },
  content: {
    marginLeft: 16,
    flex: 1,
  },
  contentHeaderComment: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 6,
  }, time: {
    fontSize: 11,
    color: "#808080",
  },
  name2: {
    fontSize: 16,
    fontWeight: "bold",
  },textSeeComments: {
    fontSize: 15,
    color: "grey",
  },
  imageComment: {
    width: 40,
    height: 44,
    borderRadius: 20,
    marginLeft: 20,
  },leftSwipeItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 20
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20
  },separator: {
    height: 1,
    backgroundColor: "#CCCCCC",
  }
});

class Article extends Component {

    state = {
        message: "",
        showMessages: false,
        showlikes: false,
        show: false,
        showEdit: false,
        title: "",
        description: "",
        reward: false,
          leftActionActivated: false,
          toggle: false,
          commentsList:[]
     
      };
      
      componentDidMount() {
        this.setState({ title: this.props.route.params.post.title });
        this.setState({ description: this.props.route.params.post.description });
        this.setState({ reward: this.props.route.params.post.reward });
        // this.setState({ commentsList: this.props.route.params.post.comments });
    
    
            const {navigation} = this.props;
        navigation.addListener ('focus',  () =>{
          this.setState({ commentsList: this.props.route.params.post.comments });
    
        });  
      }
  scrollX = new Animated.Value(0);

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <View style={[styles.flex, styles.row, styles.header]}>
          <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
            <FontAwesome name="chevron-left" color={theme.colors.white} size={theme.sizes.font * 1} />
          </TouchableOpacity>
          <TouchableOpacity>
            <MaterialIcons name="more-horiz" color={theme.colors.white} size={theme.sizes.font * 1.5} />
          </TouchableOpacity>
        </View>
      ),
      headerTransparent: true,
    }
  }

  renderDots = () => {
    const { navigation , route} = this.props;
    const post = route.params.post;
    const dotPosition = Animated.divide(this.scrollX, width);
  const opacity = dotPosition.interpolate({
            inputRange: [0 - 1, 0, 0 + 1],
            outputRange: [0.5, 1, 0.5],
            extrapolate: 'clamp'
          })
    return (
      <View style={[ styles.flex, styles.row, styles.dotsContainer ]}>
      
         
            <Animated.View
              key={`step-${1}-${0}`}
              style={[styles.dots, { opacity }]}
            />
          
       
      </View>
    )
  }
  renderComments = (post) => {
    const {leftActionActivated, toggle} = this.state;

    if (post !== undefined) {
      // return post.comments.map((comment, i) => {
        return this.state.commentsList.map((comment, i) => {


        // console.log(comment);
        
        return (
        <Swipeable
        key={`commet${i}`}
          leftActionActivationDistance={200}
          leftContent={(
    <View style={[styles.leftSwipeItem, {backgroundColor: leftActionActivated ? 'lightgoldenrodyellow' : 'red'}]}>

              {leftActionActivated ?
                <Text>release!</Text> :
                <Text>Delete!</Text>}
            </View>
          )}
          onLeftActionActivate={() => this.setState({leftActionActivated: true})}
          onLeftActionDeactivate={() => this.setState({leftActionActivated: false})}
         onLeftActionComplete={() => {

          this.setState({
            commentsList: this.state.commentsList.filter((item, i) => 
            item._id !== comment._id )
          });
          this.props.deleteComment(post._id,comment._id) }}> 
    
          <View style={styles.container2} >
      
              <Image
                style={styles.imageComment}
                source={{ uri: comment.postedBy.avatar }}
              />
            <View style={{   marginLeft: 16, flex: 1}}>
              <View style={styles.contentHeaderComment}>
                <Text style={styles.name2}>
                  {comment.postedBy.first_name} {comment.postedBy.last_name}
                </Text>
                <Text style={styles.time}>
                  {moment(comment.created).fromNow()}
                </Text>
              </View>
              <Text rkType="primary3 mediumLine"> {comment.text}</Text>
              <View style={styles.separator} />
            </View>
          </View>
         </Swipeable>
       );
      });
    
    }
  };
  renderLikeCommentsNumber = (post) => {
    if (!this.state.showlikes || !this.state.showMessages) {
      return (
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={{ marginLeft: 2 }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textSeeComments}>
                {post.likes_number} likes |
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ marginLeft: 8 }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textSeeComments}>
                {/* {post.comments_number} comments */}
                {this.state.commentsList.length} comments
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          style={{ marginLeft: 15 }}
        >
          <View>
            <Text style={styles.textSeeComments}>Close comments</Text>
          </View>
        </TouchableOpacity>
      );
    }
  };

  renderRatings = (rating) => {
    const stars = new Array(5).fill(0);
    return (
      stars.map((_, index) => {
        const activeStar = Math.floor(rating) >= (index + 1);
        return (
          <FontAwesome
            name="star"
            key={`star-${index}`}
            size={theme.sizes.font}
            color={theme.colors[activeStar ? 'active' : 'gray']}
            style={{ marginRight: 4 }}
          />
        )
      })
    )
  }

  render() {
    const { navigation ,route} = this.props;
    const post = route.params.post;

    return (
        <ScrollView showsVerticalScrollIndicator={false}>

      <View style={styles.flex}>
        <View style={[styles.flex]}>
          <ScrollView
            horizontal
            pagingEnabled
            scrollEnabled
            showsHorizontalScrollIndicator={false}
            decelerationRate={0}
            scrollEventThrottle={16}
            snapToAlignment="center"
            onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: this.scrollX } } }])}
          >
          
                <Image
                  source={{ uri: route.params.post.image}}
                  resizeMode='cover'
                  style={{ width, height: width }}
                />
              
            
          </ScrollView>
          {this.renderDots()}
        </View>
        <View style={[styles.flex, styles.content]}>
          <View style={[styles.flex, styles.contentHeader]}>
            <Image style={[styles.avatar, styles.shadow]} source={{ uri: route.params.post.author.avatar }} />
            <Text style={styles.title}>{post.title}</Text>
            <View style={[
              styles.row,
              { alignItems: 'center'/* , marginVertical: theme.sizes.margin / 2 */ }
            ]}>

<View style={{ flexDirection: "row" }}>
              <Text>{"   "}</Text>

              <Ionicons
                name="ios-heart"
                size={24}
                color={
                  this.props.route.params.post.likes.indexOf(  this.props.route.params.post.author._id ) === -1? "#737888": "red"}
              />
              <Text>{"       "}</Text>
              <Ionicons name="ios-chatboxes" size={24} color="#737888" />
            </View>
            </View>
           <View style={{ alignContent: "center" }}>
              {this.renderLikeCommentsNumber(route.params.post)}
            </View>
            <TouchableOpacity>
              <Text style={styles.description}>
                {route.params.post.description.split('').slice(0, 180)}...
                <Text style={{color: theme.colors.active}}> Read more</Text>
              </Text>
            </TouchableOpacity>
            <View >

            </View>
          </View>
        </View>
              {this.renderComments(route.params.post)}
              <Adresse location={ route.params.post.location}/>
      </View>
      </ScrollView>
    )
  }
}

export default connect(null, {
    deletePost,
    editPost,
    deleteComment
  })(Article);
  