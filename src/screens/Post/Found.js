import React, { Component } from 'react';
import { View, Text } from 'react-native';
import GestureRecognizer from "react-native-swipe-gestures";

export default class Found extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
	
  }
  onSwipeLeft(gestureState) {
    console.log('right')
    this.props.navigation.navigate("ScanQr");
  }
  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80,
    };
    return (
      <GestureRecognizer
      onSwipeLeft={(state) => this.onSwipeLeft(state)}
      config={config}
      style={{
        flex: 1,
        backgroundColor: this.state.backgroundColor,
      }}
    >
           <View   style={{
         flex: 1,
         backgroundColor: "#EFECF4",
      }}>

        <Text> Found </Text>
      </View>
      </GestureRecognizer>

    );
  }
}
