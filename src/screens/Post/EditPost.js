import React from 'react';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { editPost } from '../../actions/post.actions'
import {
    ActivityIndicator,

	
    Clipboard,
    Image,
    Share,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View, KeyboardAvoidingView, ScrollView, Picker, TextInput, AsyncStorage
} from 'react-native';
import {
    Container,
    Content,
    Header,
    Form,
    Input,
    Item,

    Label
} from "native-base";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { RadioButton } from "react-native-paper";
import uuid from 'uuid';
import * as firebase from 'firebase';
import Constants from "expo-constants";
import { Root, Toast, Popup } from 'popup-ui'
import { connect } from "react-redux";

//console.disableYellowBox = true;

const url =
    'https://console.firebase.google.com/u/2/project/pfe-auth/storage/pfe-auth.appspot.com/files'

const firebaseConfig = {
    apiKey: "AIzaSyCDSo8s66lp3WK5q_O3O9lC8kV1zworLps",
    authDomain: "pfe-auth.firebaseapp.com",
    databaseURL: "https://pfe-auth.firebaseio.com",
    projectId: "pfe-auth",
    storageBucket: "pfe-auth.appspot.com",
    messagingSenderId: "518460442619",
    appId: "1:518460442619:web:50bc23ce9e254e249c6d5e",
    measurementId: "G-YWWK60K48S"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

class EditPost extends React.Component {
    

    state = {

        title: this.props.route.params.title,
        description: this.props.route.params.description,
        id: this.props.route.params._id 
    }










    submit = () => {
        this.props.editPost(this.state.title, this.state.description, this.state.id);

        this.setState({
            title: "",
            description: "",
            id: ""
        })

        this.props.navigation.navigate("Tab")

    }


    render() {


        return (
      <Root>

          
                    <Form >
                            <View>
                                <View >
                                    <TextInput style={{ flex: 3 }}
                                        placeholder="Title"
                                        value={this.state.title}
                                        onChangeText={(title) => this.setState({ title })}

                                    />
                                    <TextInput style={{ flex: 3 }}
                                        underlineColorAndroid="transparent"
                                        placeholder={"Description"}
                                        placeholderTextColor={"#9E9E9E"}
                                        value={this.state.description}
                                        onChangeText={description => this.setState({ description })}
                                        numberOfLines={10}
                                        multiline={true}

                                    />
                                    <Button
                                        onPress={() => AsyncStorage.getItem('app_token').then(alert)}

                                        title={'CheckToken'}
                                    />
                         <Button title="Submit" onPress={this.submit} />
                                </View>
                                </View>

                    </Form>

                
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
<TouchableOpacity
          onPress={() => 
            Toast.show({
              title: 'User created',
              text: 'Your user was successfully created, use the app now.',
              color: '#2ecc71',
              timing: 2000,
              icon: <Image source={require('../../images/home.png')} style={{ width: 25, height: 25 }} resizeMode="contain" />
            })
          }
        >
          <Text>Toast Success</Text>
        </TouchableOpacity>
</View>
</Root>









        );
    }

}
 export default connect(null, {editPost})(EditPost);



