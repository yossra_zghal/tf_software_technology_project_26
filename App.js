import React from 'react';

import {
  Mapping,
  Theme,

} from "./src/screens/template/theme.service";
import { AppStorage } from "./src/screens/template/app-storage.service";
import { SplashImage } from "./src/screens/template/splash-image.component";

 import {
   AppLoading,
   LoadFontsTask,
 } from "./src/screens/template/app-loading.component";

const Splash = ({ loading }) => (
  <SplashImage loading={loading} source={require("./assets/icon.png")} />
);
const loadingTasks = [
  () =>
    LoadFontsTask({
      "opensans-regular": require("./assets/font/argon.ttf"),
      "roboto-regular": require("./assets/font//argon.ttf"),
    }),
  () =>
    AppStorage.getMapping(defaultConfig.mapping).then((result) => [
      "mapping",
      result,
    ]),
  () =>
    AppStorage.getTheme(defaultConfig.theme).then((result) => [
      "theme",
      result,
    ]),
];
const defaultConfig: { mapping: Mapping, theme: Theme } = {
  mapping: "eva",
  theme: "light",
};
export default () => (
  <AppLoading
    tasks={loadingTasks}
    initialConfig={defaultConfig}
    placeholder={Splash}
  >
    {(props) => <App {...props} />}
  </AppLoading>
);

